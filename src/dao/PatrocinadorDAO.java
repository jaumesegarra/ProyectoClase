/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Patrocinador;
import utils.BDconnect;

/**
 * Patrocinador DAO
 * @version 0.1
 * @see Patrocinador
 * @author Jaume Segarra
 */
public class PatrocinadorDAO {
    
    public static Patrocinador load(String nombre){
        return PatrocinadorDAO.load(nombre, null);
    }
    
    /**
     * Cargar datos de un patrocinador dado su nombre
     * @param nombre Nombre del patrocinador
     * @return Datos del patrocinador
     */
    public static Patrocinador load(String nombre, Connection con){
        Patrocinador p = null;
        boolean new_con = true;
        if(con != null)
            new_con = false;
        else
            con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Patrocinador WHERE nombre='"+nombre+"'"); // Consultar datos del patrocinador
            if (rs.next()) //Recorrer resultado consulta
                p = new Patrocinador(rs.getString("nombre"), rs.getString("direccion"), rs.getString("persona_contacto"), rs.getInt("telefono")); // Crear objeto con los datos del patrocinador
            st.close(); // Cerrar query
            
            if(new_con)
                con.close(); // Cerrar conexión con la BD
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return p;
    }
    
    /**
     * Cargar datos de todos los patrocinadores
     * @return Array con los patrocinadores
     */
    public static ArrayList<Patrocinador> loadAll(){
        ArrayList<Patrocinador> ps = new ArrayList<Patrocinador>();
        
        Connection con = BDconnect.connect(); // Establecer conexión con la BD 
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Patrocinador"); // Consultar datos de los patrocinadores
            while (rs.next()) //Recorrer resultado consulta
                ps.add(new Patrocinador(rs.getString("nombre"), rs.getString("direccion"), rs.getString("persona_contacto"), rs.getInt("telefono"))); // Añadir patrocinador al Array
            st.close(); // Cerrar query
            
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);  //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return ps;
    }
    
    /**
     * Guardar un nuevo patrocinador o actualizar uno ya existente 
     * @param p Patrocinador a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Patrocinador p){
        boolean saved = false;
                
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            Statement st = con.createStatement();
            
            /*
                Comprobar si existe el patrocinador en la bd
            */
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Patrocinador WHERE nombre='"+p.getNombre()+"'"); // Consultar número de patrocinadores con ese nombre en la BD
            
            int exists = 0;
            while (rs.next())
                exists = rs.getInt(1); // Cargar número
            rs.close();
            
            String telefono = (p.getTelefono() != 0) ? p.getTelefono()+"" : "NULL";
            
            if(exists==0){ //La carrera no existe en la BD (Almacenar)
                String query = "INSERT INTO Patrocinador (nombre, direccion, persona_contacto, telefono)"
                + " values (?, ?, ?, ?)";

                PreparedStatement preparedStmt = BDconnect.connect().prepareStatement(query);
                preparedStmt.setString (1, p.getNombre());
                preparedStmt.setString (2, p.getDireccion());
                
                if(!p.getPersonaContacto().trim().isEmpty())
                    preparedStmt.setString (3, p.getPersonaContacto());
                else
                    preparedStmt.setNull(3, 0);
                if(!telefono.equals("NULL"))
                    preparedStmt.setInt (4, p.getTelefono());
                else
                    preparedStmt.setNull(4, 0);
                
                preparedStmt.execute(); // Guardar datos del patrocinador
                preparedStmt.close(); // Cerrar query
                
            }else{ // La carrera ya existe en la BD (Actualizar)
                Statement st_actualizarDatosPatrocinador = con.createStatement();
                String query2 = "UPDATE Patrocinador SET direccion='"+p.getDireccion()+"', persona_contacto='"+p.getPersonaContacto()+"', telefono="+telefono+" WHERE nombre='"+p.getNombre()+"'";
                st_actualizarDatosPatrocinador.executeUpdate(query2); // Actualizar datos del patrocinador
                st_actualizarDatosPatrocinador.close(); // Cerrar query
                
            }
            
            saved=true; // Marcar como correcta
            con.close(); // Cerrar conexión con la BD
        } catch (SQLException ex) {
            saved = false; // Marcar como fallida
            
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        System.out.println(p.toString()); // Mostrar datos del patrocinador en la terminal
        
        return saved;
    }
    
    /**
     * Eliminar un patrocinador
     * @param nombre Nombre del patrocinador
     */
    public static void delete(String nombre){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try{
            Statement st_eliminarPatrocinador = con.createStatement();
            String query = "DELETE FROM Patrocinador WHERE nombre='"+nombre+"'";
            st_eliminarPatrocinador.executeUpdate(query); // Eliminar patrocinador
            st_eliminarPatrocinador.close(); // Cerrar query
            
            con.close(); // Cerrar conexión con la BD
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
}
