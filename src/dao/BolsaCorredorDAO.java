/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.BolsaCorredor;
import models.GrupoRegalo;
import models.Regalo;
import utils.BDconnect;

/**
 * Bolsa Corredor DAO
 * @version 0.1
 * @see BolsaCorredor
 * @author Jaume Segarra
 */
public class BolsaCorredorDAO {
    
    /**
     * Cargar regalos para una carrera
     * @param categoriaCarrera Categoría de la carrera
     * @param sexoCarrera Sexo de la carrera
     * @return Datos de la bolsa del corredor
     */
    public static BolsaCorredor load(String categoriaCarrera, String sexoCarrera){
        return BolsaCorredorDAO.load(categoriaCarrera, sexoCarrera, null);
    }
    
    /**
     * Cargar regalos para una carrera
     * @param categoriaCarrera Categoría de la carrera
     * @param sexoCarrera Sexo de la carrera
     * @param con Conexión a la BD
     * @return Datos de la bolsa del corredor
     */
    protected static BolsaCorredor load(String categoriaCarrera, String sexoCarrera, Connection con){
        boolean commit_ = false;
        if (con != null)
            commit_=true; //Si se pasa una conexión, debe ser para un commit
        else
            con = BDconnect.connect();
        
        BolsaCorredor bc = null;
        try{
            if(commit_)
                con.setAutoCommit(false);
            
            /*
                Obtener regalos de a bolsa de corredor
            */
            Statement sttm = con.createStatement();
            ResultSet rs = sttm.executeQuery("SELECT * FROM RegaloBolsaCorredor WHERE categoriaCarrera='"+categoriaCarrera+"' AND sexoCarrera='"+sexoCarrera+"'"); 
            ArrayList<GrupoRegalo> gp = new ArrayList<GrupoRegalo>();
            while(rs.next()){
                gp.add(GrupoRegaloDAO.load(rs.getInt("id"), con));
            }
            bc = new BolsaCorredor(categoriaCarrera, sexoCarrera, gp);
            sttm.close();
            
            if(!commit_)
                con.close();
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) {
                    if(commit_){
                        con.rollback();
                        con.setAutoCommit(true);
                    }
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return bc;
    }
    
    /**
     * Escoger los regalos a dar a un corredor
     * @param categoriaCarrera Categoría de la carrera
     * @param sexoCarrera Sexo de la carrera
     * @return Array con las opcion de cada regalo que le ha tocado al corredor
     */
    public static ArrayList<Regalo> darRegalosCorredor(String categoriaCarrera, String sexoCarrera){
        Connection con = BDconnect.connect();
        ArrayList<Regalo> regalos = new ArrayList<Regalo>();
        
        try{
            BolsaCorredor bc = BolsaCorredorDAO.load(categoriaCarrera, sexoCarrera, con); // Cargar regalos de la bolsa de corredor

            for(GrupoRegalo gr : bc.getGrupoRegalos()){ //Recorrer regalos de la bolsa de corredor
                Regalo reg = GrupoRegaloDAO.darRegaloCorredor(gr, con); //Escoger la opción del regalo
                if(reg!=null) //Si la opción no es quedarse sin regalo
                    regalos.add(reg); //añadimo el regalo al array
            }

            con.commit();
            con.setAutoCommit(true);
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) {
                    con.rollback();
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return regalos;
    }
}
