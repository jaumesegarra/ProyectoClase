/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Aportacion;
import models.AportacionObjeto;
import models.Patrocinador;
import utils.BDconnect;

/**
 * Aportación Objeto DAO
 * @version 0.1
 * @see AportacionObjeto
 * @author Jaume Segarra
 */
public class AportacionObjetoDAO {
    
    /**
     * Cargar una aportación de tipo Objeto
     * @param id Id 
     * @return Datos de la aportación
     */
    public static AportacionObjeto load(int id){
        return AportacionObjetoDAO.load(id, null);
    }
    
    /**
     * Cargar una aportación de tipo Objeto
     * @param id Id 
     * @param con Conexión a la BD
     * @return Datos de la aportación
     */
    protected static AportacionObjeto load(int id, Connection con){
        AportacionObjeto ap = null;
        boolean new_con = true; // Si es una nueva conexión a la BD
        
        if(con !=  null)
            new_con = false;
        else //Si no se ha pasado ninguna conexión a la BD
            con = BDconnect.connect(); //se crea una nueva
        
        try{
            
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT don.*, dono.cantidad, dono.cantidad_disponible FROM Donacion don, DonacionObjeto dono WHERE don.id="+id+" AND don.id=dono.idDonacion");
            
            if (rs.next())
            {
                Patrocinador aportador = PatrocinadorDAO.load(rs.getString("nombrePatrocinador"), con); // Cargar datos del aportador
                
                ap = new AportacionObjeto(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getInt("cantidad"), rs.getInt("cantidad_disponible")); 
                
                ap.setCantidad_no_endeudada(AportacionObjetoDAO.calculateNE(ap, con)); // Calcular cantidad No Endeudada
            }
            
            st.close();
            
            if(new_con) //Si era una nueva conexión
                con.close(); //se cierra
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
                
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return ap;
    }
    
    /**
     * Cargar todas las aportaciones de tipo objeto
     * @return Array con las aportaciones
     */
    public static ArrayList<Aportacion> loadAll(){
        ArrayList<Aportacion> al = new ArrayList<Aportacion>();
        
        Connection con = BDconnect.connect();
        try{
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT d.*, dob.cantidad, dob.cantidad_disponible FROM DonacionObjeto dob, Donacion d WHERE dob.idDonacion = d.id");
            
            while (rs.next())
            {
                Patrocinador aportador = PatrocinadorDAO.load(rs.getString("nombrePatrocinador"), con); //Cargar datos del patrocinador
                
                AportacionObjeto apo = new AportacionObjeto(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getInt("cantidad"), rs.getInt("cantidad_disponible"));
                
                al.add(apo); // Añadir aportación al Array
            }
            
            rs.close();
            con.close();
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
                
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return al;
    }
    
    /**
     * Calcular cantidad No enduedada de una aportación dada
     * @param apo Aportación a calcular NE
     * @param con Conexión a la BD
     * @return Cantidad NE
     */
    protected static int calculateNE(AportacionObjeto apo, Connection con){
        int ne = 0;
        
        try{
            Statement stCantNE = con.createStatement();
            ResultSet rsCantNE = stCantNE.executeQuery("SELECT SUM(cantidad_disponible) AS ende FROM RegaloBolsaCorredorOpcion WHERE idDonacion="+apo.getId()); // Consultar cantidad no endeudada (No repartida a ningúna bolsa de corredor)
            if(rsCantNE.next()){
                ne = apo.getCantidadDisponible()-rsCantNE.getInt(1); //calcula a pertir de la obtenido de la BD y la cantidad disponible de la aportación
            }
            rsCantNE.close(); // Cerrar query
            
        }catch(SQLException e){
            BDconnect.showMYSQLerrors(e);
            
            try{
                if (con != null) 
                    con.close();
                
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return ne;
    }
    
    /**
     * Guardar una nueva aportación o modificar una existente
     * @param ap Aportacion a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Aportacion ap){
        Connection con = BDconnect.connect();
        boolean saved = AportacionDAO.save(ap,con);
        
        if(saved){
            
            try {
                Statement st = con.createStatement();

                /*
                    Comprobar si existe la aportación en la bd
                */
                ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM DonacionObjeto WHERE idDonacion="+ap.getId());

                int exists = 0;
                if (rs.next()) {
                    exists = rs.getInt(1);
                }
                rs.close();

                if(exists==0){ //La aportación no existe en la BD (Almacenar)
                    String query = "INSERT INTO DonacionObjeto (idDonacion, cantidad, cantidad_disponible)"
                    + " values (?, ?, ?)";

                    PreparedStatement preparedStmt = con.prepareStatement(query);
                    preparedStmt.setInt (1, ap.getId());
                    preparedStmt.setInt(2, ((AportacionObjeto)ap).getCantidad());
                    preparedStmt.setInt(3, ((AportacionObjeto)ap).getCantidadDisponible());
                    preparedStmt.execute();
                    
                    preparedStmt.close();
                    
                }else{
                    Statement st_actualizarDatosAportacion = con.createStatement();
                    String query2 = "UPDATE DonacionObjeto SET cantidad= cantidad +"+((AportacionObjeto)ap).getCantidad()+", cantidad_disponible= cantidad_disponible+"+((AportacionObjeto)ap).getCantidad()+"  WHERE idDonacion="+ap.getId(); //Sumar o restar a la cantidad actual de la aportación
                    st_actualizarDatosAportacion.executeUpdate(query2);
                    st_actualizarDatosAportacion.close();
                    
                    /*
                        Obtener nueva cantidad de la aportación
                    */
                    Statement st_c = con.createStatement();
                    ResultSet rs_c = st_c.executeQuery("SELECT cantidad FROM DonacionObjeto WHERE idDonacion="+ap.getId());

                    if (rs_c.next()) {
                        ((AportacionObjeto)ap).setCantidad(rs_c.getInt(1));
                    }
                    rs_c.close();
                }
                
                saved=true;
                con.commit();
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException ex) {
                saved=false;
                
                BDconnect.showMYSQLerrors(ex);
                
                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
            }
        }
        
        System.out.println(((AportacionObjeto)ap).toString());
        
        return saved;
    }
}
