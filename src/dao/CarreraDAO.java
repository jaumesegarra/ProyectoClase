/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import models.Carrera;
import models.Recorrido;
import models.Regla;
import utils.BDconnect;

/**
 * Carrera DAO
 * @version 0.1
 * @see Carrera
 * @author jaumesegarra
 */
public class CarreraDAO {
    
    /**
     * Cargar datos de una carrera en especifico
     * @param categoria Categoria
     * @param sexo Sexo
     * @return Objeto con los datos
     */
    public static Carrera load(String categoria, String sexo){
        return CarreraDAO.load(categoria, sexo, null);
    }
            
    /**
     * Cargar datos de una carrera en especifico
     * @param categoria Categoria
     * @param sexo Sexo
     * @param con Conexión a la BD
     * @return Objeto con los datos
     */
    protected static Carrera load(String categoria, String sexo, Connection con){
        Carrera c = null;
        boolean new_con = true;
        
        if(con !=  null)
            new_con = false;
        else
            con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Carrera WHERE categoria='"+categoria+"' AND sexo='"+sexo+"'"); // Consultar datos carrera en la BD
            
            if (rs.next()) //Recorrer resultado consulta
            {
                Recorrido recorrido_ = null;
                
                if(!rs.getString("codigoRecorrido").equals("")){ // Si hay un recorrido asignado a la carrera
                    recorrido_ = RecorridoDAO.load(rs.getString("codigoRecorrido"), con); //Carga los datos del recorrido
                }

                ArrayList<Regla> reglas_ = ReglaDAO.loadFromCarrera(rs.getString("categoria"), rs.getString("sexo"), con); //Carga las reglas de la carrera
                
                c = new Carrera(rs.getString("hora_comienzo"), reglas_, recorrido_, rs.getString("categoria"), rs.getString("sexo")); //Crea el objeto a devolver con todos los datos de la carrera
            }
            
            st.close(); // Cerrar consulta
            
            if(new_con)
                con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return c;
    }
    
    /**
     * Cargar todas la carreras y sus datos
     * @return Array con las carreras
     */
    public static ArrayList<Carrera> loadAll(){
        ArrayList<Carrera> c = new ArrayList<Carrera>();
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = BDconnect.connect().createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Carrera"); // Consultar datos de las carreras
            
            while (rs.next()) // Recorre resultados de la consulta
            {
                Recorrido recorrido_ = null;
                
                if(!rs.getString("codigoRecorrido").equals("")){ // Si hay un recorrido asignado a la carrera
                    recorrido_ = RecorridoDAO.load(rs.getString("codigoRecorrido"), con); // Carga los datos del recorrido
                }
                
                ArrayList<Regla> reglas_ = ReglaDAO.loadFromCarrera(rs.getString("categoria"), rs.getString("sexo"), con); //Carga las reglas de la carrera
                
                Carrera car = new Carrera(rs.getString("hora_comienzo"), reglas_, recorrido_, rs.getString("categoria"), rs.getString("sexo")); // Crea el objeto carrera con los datos
                car.setAprobada(rs.getInt("aprobada")); // Define si la carrera esta aprobada o no
                c.add(car); //Añade la carrera al Array
            }
            
            st.close(); // Cerrar consulta
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {            
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return c;
    }
    
    /**
     * Devuelve las categorías que aún no se han registrado en la BD (de hombres y/o mujeres)
     * @return Array con las categorías no registradas aún
     */
    public static ArrayList<String[]> loadCarrerasPorCrear(){
        ArrayList<String[]> cpc = new ArrayList<String[]>(); // Array con todas las categorias de carrera y is han sido registradas para hombres y mujeres
        ArrayList<String[]> cpc_ = new ArrayList<String[]>(); // Array con las carreras que han sido registradas (de hombres y mujeres)
        cpc.add(new String[]{"Benjamín","0","0"});
        cpc.add(new String[]{"Alevín","0","0"});
        cpc.add(new String[]{"Infantil","0","0"});
        cpc.add(new String[]{"Cadete","0","0"});
        cpc.add(new String[]{"Juvenil","0","0"});
        cpc.add(new String[]{"Senior","0","0"});
        cpc.add(new String[]{"Veterano","0","0"});
        cpc.add(new String[]{"minibenjamín","0","0"});
        cpc.add(new String[]{"chupetines","0","0"});
        
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT categoria, sexo FROM Carrera"); // Consultar cuales han sido registradas en la BD
            
            while (rs.next()) // Recorrerlas
            {
                int poss=0; // Posicion del Array con todas las categorías
                for(String[] cat : cpc){ // Recorrer Array con todas las categorías
                    if(cat[0].equals(rs.getString("categoria"))){ // comprobando si es la misma categoría que la de la BD
                        cat[((rs.getString("sexo").equals("HOMBRE")) ? 1:2)] = "1"; // marcar como registrada la categoria en su sexo en el Array
                        if(cat[1].equals("1") && cat[2].equals("1")) // si ya están marcadas como registradas para los dos sexos
                            cpc_.add(cat); // se añaden al Array de registradas
                        else
                            cpc.set(poss, cat); // se substituye la cetgoria en el Array con todas, con la categoria marcada como registrada en su sexo
                    }
                    poss++;
                }
            }
            
            st.close(); // Cerrar consulta
            con.close(); // Cerrar conexión
            cpc.removeAll(cpc_); // Eliminar registradas del Array con todas las categorias
            
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); // Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return cpc;
    }
    
    /**
     * Marca como aprobada o desaprobada una carrera dada
     * @param categoria Categoria
     * @param sexo Sexo
     * @param ap Marcar como Aprobada (true) o Desaprobada (false)
     */
    public static void saveAprobar(String categoria, String sexo, boolean ap){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            Statement st_actualizarApCarrera = con.createStatement();
            String query2 = "UPDATE Carrera SET aprobada="+((ap)? "1": "0")+" WHERE categoria='"+categoria+"' AND sexo='"+sexo+"'"; // Actualizar campo 'aprobada' de la carrera
            st_actualizarApCarrera.executeUpdate(query2);
            
            con.close(); // Cerrar conexión
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); // Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
    
    /**
     * Guardar una nueva carrera o actualizar una ya existente
     * @param car Carrera a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Carrera car){
        boolean saved = false;
        
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            con.setAutoCommit(false); // Deshabilitar el AUTOCOMMIT de MYSQL
            
            Statement st = con.createStatement();
            
            /*
                Comprobar si existe la carrera en la bd
            */
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Carrera WHERE categoria='"+car.getCategoria()+"' AND sexo='"+car.getSexo()+"'"); // Consultar número de carreras en la BD con esa categoris y sexo
            
            int exists = 0;
            while (rs.next()) {
                exists = rs.getInt(1); // Cargar número
            }
            rs.close();
            
            if(exists==0){ //La carrera no existe en la BD (Almacenar)
                String query = "INSERT INTO Carrera (hora_comienzo, categoria, sexo, codigoRecorrido)"
                + " values (?, ?, ?, ?)";
                
                PreparedStatement preparedStmt = con.prepareStatement(query);
                preparedStmt.setString (1, car.getHoraComienzo());
                preparedStmt.setString (2, car.getCategoria());
                preparedStmt.setString (3, car.getSexo());
                preparedStmt.setString (4, car.getRecorrido().getCodigo());

                preparedStmt.execute(); // Guardar datos de la carrera
                preparedStmt.close(); // Cerrar query
                
                if(!car.getReglas().isEmpty()){ // Si la carrera tiene reglas asignadas
                    Statement st2 = con.createStatement();

                    String query2 = "INSERT INTO ReglaCarrera (categoriaCarrera, sexoCarrera, idRegla) VALUES ";

                    int i = 0;
                    for(Regla r : car.getReglas()){
                        if(i!=0)
                            query2+=",";
                        query2+="('"+car.getCategoria()+"', '"+car.getSexo()+"', "+r.getId()+")";
                        i++;
                    }

                    st2.executeUpdate(query2); // Insertar reglas a la carrera
                    st2.close(); // Cerrar query
                }                
            }else{ // La carrera ya existe en la BD (Actualizar)
                Statement st_actualizarDatosCarrera = con.createStatement();
                String query2 = "UPDATE Carrera SET hora_comienzo='"+car.getHoraComienzo()+"', codigoRecorrido='"+car.getRecorrido().getCodigo()+"' WHERE categoria='"+car.getCategoria()+"' AND sexo='"+car.getSexo()+"'";
                st_actualizarDatosCarrera.executeUpdate(query2); // Actualizar datos de la carrera
                st_actualizarDatosCarrera.close(); // Cerrar query
                
                Statement st_eliminarReglasPreviasCarrera = con.createStatement();
                String query3 = "DELETE FROM ReglaCarrera WHERE categoriaCarrera='"+car.getCategoria()+"' AND sexoCarrera='"+car.getSexo()+"'";
                st_eliminarReglasPreviasCarrera.executeUpdate(query3); // Eliminar todas las reglas asignadas a la carrera
                st_eliminarReglasPreviasCarrera.close(); // Cerrar query
                
                if(!car.getReglas().isEmpty()){ // Si la carrera tiene reglas asignadas
                    Statement st_insertarReglasCarrera = con.createStatement();

                    String query4 = "INSERT INTO ReglaCarrera (categoriaCarrera, sexoCarrera, idRegla) VALUES ";

                    int i = 0;
                    for(Regla r : car.getReglas()){
                        if(i!=0)
                            query4+=",";
                        query4+="('"+car.getCategoria()+"', '"+car.getSexo()+"', "+r.getId()+")";
                        i++;
                    }

                    st_insertarReglasCarrera.executeUpdate(query4); // Insertar nuevas reglas a la carrera
                    st_insertarReglasCarrera.close(); // Cerrar query
                }
            }
            con.commit(); // Hacer commit de los querys
            saved=true; // Marcar como correcta
            
            con.setAutoCommit(true); // Habilitar el AUTOCOMMIT de MYSQL
            con.close(); // Cerrar conexión con la BD
            
        } catch (SQLException ex) {
             saved = false; // Marcar como fallida
             
            if(ex.getMessage().contains("Duplicate entry") && ex.getMessage().contains("hora_comienzo")) //Si ya hay una carrera con el mismo recorrido y a la misma hora de comienzo
                JOptionPane.showMessageDialog(null, "Ya existe una carrera asignada a ese recorrido \ncon la misma hora de comienzo", "Error", JOptionPane.ERROR_MESSAGE); // Mostrar error
            else
                BDconnect.showMYSQLerrors(ex); // Muestra los errores SQL
            
            try{
                if (con != null) { // Si no se ha cerrado la conexión
                    con.rollback(); // Hacer rollback de los querys
                    con.setAutoCommit(true); // Habilitar el AUTOCOMMIT de MYSQL
                    con.close(); // Cerrar conexión
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        System.out.println(car.toString()); // Mostrar datos de la carrera en la terminal
        
        return saved;
    }
    
    
    /**
     * Eliminar una carrera dada
     * @param categoria Categoria
     * @param sexo Sexo
     */
    public static void delete(String categoria, String sexo){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try{
            Statement st_eliminarCarrera = con.createStatement();
            String query = "DELETE FROM Carrera WHERE categoria='"+categoria+"' AND sexo='"+sexo+"'";
            st_eliminarCarrera.executeUpdate(query); // Eliminar carrera
            st_eliminarCarrera.close(); // Cerrar query
            
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); // Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // Cerrar conexión
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
}
