/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import models.Carrera;
import models.GrupoRegalo;
import models.Regalo;
import utils.BDconnect;

/**
 * Grupo regalo DAO
 * @version 0.1
 * @see GrupoRegalo
 * @author Jaume Segarra
 */
public class GrupoRegaloDAO {
    
    /**
     * Cargar regalo dado su id
     * @param id Id del regalo
     * @return Regalo
     */
    public static GrupoRegalo load(int id){
        return GrupoRegaloDAO.load(id, null);
    }
    
    /**
     * Cargar regalo dado su id
     * @param id Id del regalo
     * @param con Conexión con la BD
     * @return Regalo
     */
    public static GrupoRegalo load(int id, Connection con){
        GrupoRegalo gr = null;
        
        boolean commit_ = false;
        if (con != null) //Si se pasa una conexion
            commit_=true; //será para hacer una transacción
        else
            con = BDconnect.connect();
        
        try{
            if(commit_)
                con.setAutoCommit(false);
            
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT id, periodo, categoriaCarrera, sexoCarrera FROM RegaloBolsaCorredor WHERE id="+id); //Obtener datos del Regalo
            
            if(rs.next()){
                Carrera c = CarreraDAO.load(rs.getString("categoriaCarrera"), rs.getString("sexoCarrera"), con); //Cargar datos de la carrera
                float gr_p = rs.getFloat("periodo");
                
                Statement stmt2 = con.createStatement();
                ResultSet rs2 = stmt2.executeQuery("SELECT * FROM RegaloBolsaCorredorOpcion WHERE idRegaloBolsaCorredor="+id); //Obtener opciones regalo
                
                gr = new GrupoRegalo(id,c, gr_p);
                while(rs2.next()){
                    gr.addRegalo(new Regalo(rs2.getInt("id"), AportacionObjetoDAO.load(rs2.getInt("idDonacion"), ((commit_) ? con : null)), rs2.getFloat("periodo_actual"), rs2.getInt("cantidad"), rs2.getInt("cantidad_disponible"), rs2.getInt("cantidad_x_participante"))); //Añadir opción al array de opciones del regalo
                }
                stmt2.close();
            }
            stmt.close();
            
            if(!commit_)
                con.close();
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) {
                    if(commit_){
                        con.rollback();
                        con.setAutoCommit(true);
                    }
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return gr;
    }
    
    /**
     * Guardar un nuevo Regao o modificar uno existente
     * @param gr Regalo a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(GrupoRegalo gr){
        boolean saved=false;
        boolean commit_ = true;
        
        Connection con = BDconnect.connect();
        try {
            con.setAutoCommit(false);
            
            if(gr.getId()==0){ //Si es un nuevo regalo
                /*
                    Insertar datos
                */
                Statement stmt_cGrupoRegalos = con.createStatement();
                stmt_cGrupoRegalos.executeUpdate("INSERT INTO RegaloBolsaCorredor (periodo, categoriaCarrera, sexoCarrera) VALUES ("+gr.getPeriodo()+", '"+gr.getCarrera().getCategoria()+"', '"+gr.getCarrera().getSexo()+"')");
                stmt_cGrupoRegalos.close();
                
                /*
                    Obtener id
                */
                Statement st_getId = con.createStatement();
                ResultSet rs_getId = st_getId.executeQuery("SELECT MAX(id) FROM RegaloBolsaCorredor");
                if(rs_getId.next()){
                    gr.setId(rs_getId.getInt(1));
                }
                rs_getId.close();

                
                /*
                    Guardar opciones del regalo
                */
                for(Regalo reg : gr.getRegalos()){
                   Statement st_insertarRegalosGrupo = con.createStatement();
                   String query_others = "INSERT INTO RegaloBolsaCorredorOpcion (idRegaloBolsaCorredor, idDonacion, cantidad, cantidad_disponible, cantidad_x_participante, periodo_actual) VALUES ";
                          query_others+="("+gr.getId()+", "+reg.getAportacion().getId()+", "+((reg.getCantidad()>0) ? reg.getCantidad() : reg.getAportacion().getCantidad())+", "+((reg.getCantidad()>0) ? reg.getCantidadDisponible() : reg.getAportacion().getCantidad())+", "+reg.getCantidadPorParticipante()+", "+reg.getPeriodoActual()+")";
                   
                   st_insertarRegalosGrupo.executeUpdate(query_others);
                   st_insertarRegalosGrupo.close();
                   
                   /*
                        Obtener id de la opción
                   */
                   Statement st_getIdRegalo = con.createStatement();
                   ResultSet rs_getIdRegalo = st_getIdRegalo.executeQuery("SELECT MAX(id) FROM RegaloBolsaCorredorOpcion");
                   if(rs_getIdRegalo.next()){
                       reg.setId(rs_getIdRegalo.getInt(1));
                   }
                   rs_getIdRegalo.close();
                   
                }
            }else{ // si no es un regalo nuevo
                for(Regalo reg : gr.getRegalos()){ //Recorrer opciones del regalo
                    if(reg.getId() == 0){ //Si es una nueva opción
                        /*
                            Insertar datos de la opción
                        */
                        Statement st_insertarRegaloGrupo = con.createStatement();
                        String query_others = "INSERT INTO RegaloBolsaCorredorOpcion (idRegaloBolsaCorredor, idDonacion, cantidad, cantidad_disponible, cantidad_x_participante, periodo_actual) VALUES ("+gr.getId()+", "+reg.getAportacion().getId()+", "+reg.getCantidad()+", "+reg.getCantidadDisponible()+", "+reg.getCantidadPorParticipante()+", "+reg.getPeriodoActual()+")";
                        st_insertarRegaloGrupo.executeUpdate(query_others);
                        st_insertarRegaloGrupo.close();
                        
                        /*
                            Obtener id de la opción
                        */
                        Statement st_getIdRegalo = con.createStatement();
                        ResultSet rs_getIdRegalo = st_getIdRegalo.executeQuery("SELECT MAX(id) FROM RegaloBolsaCorredorOpcion");
                        if(rs_getIdRegalo.next()){
                            reg.setId(rs_getIdRegalo.getInt(1));
                        }
                        rs_getIdRegalo.close();
                    }else{ // si no es una nueva opción
                        
                        /*
                            Actualizar cantidades de la opción 
                         */
                        Statement st_actualizarRegaloGrupo = con.createStatement();
                        String query_others = "UPDATE RegaloBolsaCorredorOpcion SET cantidad=cantidad+"+reg.getCantidad()+", cantidad_disponible=cantidad_disponible+"+reg.getCantidad()+", cantidad_x_participante="+reg.getCantidadPorParticipante()+" WHERE id="+reg.getId();
                        st_actualizarRegaloGrupo.executeUpdate(query_others);
                        st_actualizarRegaloGrupo.close();
                        
                        /*
                            Obtener nuevas cantidades de la opción
                        */
                        Statement st_getCantidadRegalo = con.createStatement();
                        ResultSet rs_getCantidadRegalo = st_getCantidadRegalo.executeQuery("SELECT cantidad, cantidad_disponible FROM RegaloBolsaCorredorOpcion WHERE id="+reg.getId());
                        
                        if(rs_getCantidadRegalo.next()){
                            reg.setCantidad(rs_getCantidadRegalo.getInt(1));
                            reg.setCantidadDisponible(rs_getCantidadRegalo.getInt(2));
                            
                            /*
                                Si la cantidad total o la disponible da menos de 0 no actualizará las cantidades
                            */
                            if(rs_getCantidadRegalo.getInt(1) <= 0 || rs_getCantidadRegalo.getInt(2) < 0){
                                commit_= false;
                                saved=false;
                                break;
                            }
                        }
                        rs_getCantidadRegalo.close();
                    }
                }
            }
            
            if(commit_)
                con.commit();
            else
                con.rollback();
            saved=true;
            
            con.setAutoCommit(true);
            con.close();
        } catch (SQLException ex) {
            saved=false;

            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) {
                    con.rollback();
                    con.setAutoCommit(true);
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        System.out.println(gr); // Mostrar datos del regalo
        
        return saved;
    }
    
    /**
     * Eliminar un regalo
     * @param id Id del regalo
     */
    public static void delete(int id){
        try{
            Statement st_eliminarRecorrido = BDconnect.connect().createStatement();
            String query = "DELETE FROM RegaloBolsaCorredor WHERE id="+id;
            st_eliminarRecorrido.executeUpdate(query);
            st_eliminarRecorrido.closeOnCompletion();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
        }
    }
    
    /**
     * Escoger una opción del regalo
     * @param gr Regalo
     * @param elements Número de opciones disponibles
     * @param con Conexión a la BD
     * @return Opción del regalo que ha tocado
     * @throws SQLException 
     */
    private static Regalo chooseRegalo(GrupoRegalo gr, int elements, Connection con) throws SQLException{
        Regalo reg = null; boolean repeat_method = false;
        
            con.setAutoCommit(false); //Transacción
            for(Regalo reg_f : gr.getRegalos()){ //recorrer opciones del regalo
                
                float periodoAnt = reg_f.getPeriodoActual(); //Guardar periodo de la opción
                boolean choosed = false; // si la opción es la escogida
                reg_f.setPeriodoActual(reg_f.getPeriodoActual()+gr.getPeriodo()); //actualizar periodo de la opción

                if(reg_f.getPeriodoActual() >= 0.99 && reg == null) //Si al incrementar el periodo este es 1 o se acerca mucho (opción posible para escoger)
                {
                    reg_f.setPeriodoActual(0); //actualizar periodo de la opcion a 0
                    if(reg_f.getAportacion().getCantidadDisponible() > 0 && reg_f.getCantidadDisponible() > 0){ // si queda cantidad de esa opción
                        reg = reg_f; //poner la opción en la variable a devolver
                        /*
                            Actualizar cantidades de la opción y de la aportación
                        */
                        reg_f.getAportacion().setCantidadDisponible(reg_f.getAportacion().getCantidadDisponible()-reg_f.getCantidadPorParticipante());
                        reg_f.setCantidadDisponible(reg_f.getCantidadDisponible()-reg_f.getCantidadPorParticipante());
                        
                        choosed=true; //marcar como escogida
                    }else{ // si no quedan unidades de la opción
                        elements--; //Se decrementa el numero de opciones posibles
                        repeat_method=true; // Se pide que se repita el metodo al final de este
                    }
                }
                
                /*
                    Se guarda el nuevo periodo de la opción en la BD
                */
                Statement st_cambiarPeriodoRegalo = con.createStatement();
                String query = "UPDATE RegaloBolsaCorredorOpcion SET periodo_actual= -"+reg_f.getPeriodoActual()+"+0.01 "+((choosed) ? ", cantidad_disponible= cantidad_disponible-"+reg_f.getCantidadPorParticipante() : "")+" WHERE idRegaloBolsaCorredor="+gr.getId()+" AND periodo_actual="+periodoAnt;
                st_cambiarPeriodoRegalo.executeUpdate(query);
                st_cambiarPeriodoRegalo.closeOnCompletion();
                
                if(choosed){ //Si esta opción fue la escogida, se guardan las nuevas cantidades
                    Statement st_actualizarCantidadDisponibleAportacion = con.createStatement();
                    st_actualizarCantidadDisponibleAportacion.executeUpdate("UPDATE RegaloBolsaCorredorOpcion SET cantidad_disponible= cantidad_disponible-"+reg_f.getCantidadPorParticipante()+"  WHERE idDonacion="+reg_f.getAportacion().getId());
                    st_actualizarCantidadDisponibleAportacion.closeOnCompletion(); //para  que no haya conflicto de claves repetidas, se guarda el periodo de forma negativa
                }
            }
            
            /*
                Se vuleven a pasar los periodos a positivo
            */
            Statement st_cambiarPeriodoRegalo = con.createStatement();
            String query = "UPDATE RegaloBolsaCorredorOpcion SET periodo_actual= -periodo_actual+0.01 WHERE idRegaloBolsaCorredor="+gr.getId();
            st_cambiarPeriodoRegalo.executeUpdate(query);
            st_cambiarPeriodoRegalo.closeOnCompletion();
            
            if(repeat_method && elements>0) //si le pedimos repitiera el metodo antes; y aún quedan opciones posibles para el regalo
                    reg = chooseRegalo(gr, elements, con); //repite el metodo
             
        return reg;
    }
    
    /**
     * Obtiene la opción posible según el periodo actual del regalo
     * @return Devuelve la opción posible
     */
    protected static Regalo darRegaloCorredor(GrupoRegalo gr, Connection con) throws SQLException{
        return chooseRegalo(gr, gr.getRegalos().size(), con);
    }
}
