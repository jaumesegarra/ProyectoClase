/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Carrera;
import models.Club;
import utils.BDconnect;

/**
 * Club DAO
 * @version 0.1
 * @see Club
 * @author Jaume Segarra
 */
public class ClubDAO {
    
    /**
     * Cargar datos de un club
     * @param id Id del club
     * @return datos del club
     */
    public static Club load(int id){
        return ClubDAO.load(id, null);
    }
           
    /**
     * Cargar datos de un club
     * @param id Id del club
     * @param con Conexión a la BD
     * @return datos del club
     */
    protected static Club load(int id, Connection con){
        Club c = null;
        boolean new_con = true;
        
        if(con !=  null)
            new_con = false;
        else
            con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Club WHERE id="+id);
            
            if (rs.next())
            {
                c = new Club(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion"), rs.getInt("telefono"), rs.getString("nombre_responsable"));
            }
            
            st.close();
            if(new_con)
                con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
                    
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return c;
    }
    
    /**
     * Devuelve los clubs (con su posición final) que sus corredores hayan participado en una carrera
     * @param categoria Categoría de la ccarrera
     * @param sexo Sexo de la carrera
     * @return 
     */
    public static ArrayList<Club> loadAll(String categoria, String sexo){
        ArrayList<Club> cs = new ArrayList<Club>();
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            String query = "SELECT c.*, suma_posiciones FROM Club c, (SELECT c.idClub, SUM(puesto_llegada) AS suma_posiciones FROM Persona p, Corredor c, CorredorEdadVista ce WHERE p.dni=ce.dni AND c.dniPersona=ce.dni AND ";
            int[] edad=Carrera.getEdadParticipar(categoria, sexo);
            if(edad[0] != -1)
                query+= "edad>="+edad[0]+" AND ";
            if(edad[1] != -1)
                query+= "edad<="+edad[1]+" AND ";
            query+="sexo='"+sexo+"'";
            query+=" AND puesto_llegada IS NOT NULL AND idClub IS NOT NULL GROUP BY idClub ORDER BY suma_posiciones ASC) t WHERE c.id= t.idClub";
            
            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                Club c = new Club(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion"), rs.getInt("telefono"), rs.getString("nombre_responsable"));
                c.setPos_total_corredores(rs.getInt("suma_posiciones")); //guardar total de posiciones de los corredores del club
                cs.add(c);
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
                    
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return cs;
    }
    
    /**
     * Cargar todos los clubes y sus datos
     * @return Array con los clubs
     */
    public static ArrayList<Club> loadAll(){
        ArrayList<Club> cs = new ArrayList<Club>();
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Club");
            
            while (rs.next())
            {
                cs.add(new Club(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion"), rs.getInt("telefono"), rs.getString("nombre_responsable"))); // Añadir club al array
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
                    
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return cs;
    }
    
    /**
     * Guardar un nuevo club o modificar uno ya existente
     * @param club Club a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Club club){
        boolean saved = false;
        Connection con = BDconnect.connect();
        
        try{
            con.setAutoCommit(false);
                        
            if(club.getId()==0){ //si es un nuevo club
                Statement stmt_cClub = con.createStatement();
                stmt_cClub.executeUpdate("INSERT INTO Club (nombre, direccion, telefono, nombre_responsable) VALUES ('"+club.getNombre()+"','"+club.getDireccion()+"',"+club.getTelefono()+",'"+club.getNombre_responsable()+"')");
                stmt_cClub.close();
                
                /*
                    Obtener id del nuevo club
                */
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id) FROM Club");
                if(rs.next()){
                    club.setId(rs.getInt(1));
                }
                rs.close();
                
            }else{ // si no lo es
                Statement stmt_aClub = con.createStatement();
                stmt_aClub.executeUpdate("UPDATE Club SET nombre='"+club.getNombre()+"', direccion='"+club.getDireccion()+"', telefono="+club.getTelefono()+", nombre_responsable='"+club.getNombre_responsable()+"' WHERE id="+club.getId()+"");
                stmt_aClub.close();
            }
            
            saved=true;
            con.commit();
            con.setAutoCommit(true);
            con.close();
        } catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
        }
        
        return saved;
    }
    
    /**
     * Eliminar un club
     * @param id Id del club
     * @return True si se elminó, False si no
     */
    public static boolean delete(int id){
        boolean deleted = false;
        Connection con = BDconnect.connect();
        
        try{
            Statement stmt_rClub = con.createStatement();
            stmt_rClub.executeUpdate("DELETE FROM Club WHERE id="+id);
            stmt_rClub.close();
            
            con.close();
        }catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) 
                        con.close();
                    
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
        }
        return deleted;
    }
}
