/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Recorrido;
import utils.BDconnect;

/**
 * Recorrido DAO
 * @version 0.1
 * @see Recorrido
 * @author Jaume Segarra
 */
public class RecorridoDAO {
    
    /**
     * Comprobar si existe un recorrido con el mismo código
     * @param codigo Codigo a buscar
     * @return True si existe, False si no existe
     */
    public static boolean checkExists(String codigo){
        boolean exists = false;
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Recorrido WHERE codigo='"+codigo+"'"); // Contar recorridos registrados con el mismo código
            
            int c_exists = 0;
            while (rs.next()) {
                c_exists = rs.getInt(1); // Cargar la cuenta
            }
            exists = (c_exists !=0); // Si la cuenta no es 0 (Existe), si lo es (No existe)
            rs.close(); // Cerrar query
            con.close(); // Cerrar conexión
            
        }catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL       
                try{
                    if (con != null) // Si no se ha cerrado la conexión
                        con.close(); // la cierra
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
        }
        return exists;
    }
    
    /**
     * Cargar datos de un recorrido dado su código
     * @param codigo Código del recorrido
     * @return Datos del recorrido
     */
    public static Recorrido load(String codigo){
        return RecorridoDAO.load(codigo, null);
    }
    
    /**
     * Cargar datos de un recorrido dado su código
     * @param codigo Código del recorrido
     * @param con Conexión a la BD
     * @return Datos del recorrido
     */
    protected static Recorrido load(String codigo, Connection con){
        Recorrido r = null;
        boolean new_con = true;
        
        if(con !=  null)
            new_con = false;
        else
            con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Recorrido WHERE codigo='"+codigo+"'");
            
            if (rs.next()) //Recorrer resultado consulta
            {
                ArrayList<String> obsr = loadObservaciones(rs.getString("codigo"), con); // Cargar observaciones del recorrido
                
                r  = new Recorrido(rs.getString("codigo"), rs.getString("descripcion"), Float.parseFloat(rs.getString("kilometros")), obsr); // Crear objeto con los datos del recorrido
                r.setAprobada(rs.getInt("aprobada"));
            }
            
            st.close(); // Cerrar query
            if(new_con)
                con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return  r;
    }
    
    /**
     * Cargar observaciones de un recorrido
     * @param codigo Código del recorrido
     * @return Array con las observaciones
     * @throws SQLException 
     */
    private static ArrayList<String> loadObservaciones(String codigo, Connection con) throws SQLException{
        ArrayList<String> obsr = new ArrayList<String>();
        
        Statement st_ob = con.createStatement();
        ResultSet rs_ob = st_ob.executeQuery("SELECT * FROM RecorridoObservacion WHERE codigoRecorrido='"+codigo+"'");
        while (rs_ob.next()) //Recorrer resultado consulta
        {
            obsr.add(rs_ob.getString("observacion")); // Añadir observacion al Array
        }
        st_ob.close(); // Cerrar query
                
        return obsr;
    }
    
    /**
     * Cargar todos los recorridos y sus datos
     * @return Array con los recorridos
     */
    public static ArrayList<Recorrido> loadAll(){
        ArrayList<Recorrido> recs = new ArrayList<>();
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT * FROM Recorrido"); // Consultar datos de los recorridos
            
            while (rs.next()) // Recorrer resultado consulta
            {
                ArrayList<String> obsr = loadObservaciones(rs.getString("codigo"), con); // Cargar observaciones del recorrido
                
                Recorrido rec = new Recorrido(rs.getString("codigo"), rs.getString("descripcion"), Float.parseFloat(rs.getString("kilometros")), obsr); // Crear objeto con los datos del recorrido
                rec.setAprobada(rs.getInt("aprobada")); // Define si el recorrido esta aprobado o no
                recs.add(rec); // Añade el recorrido al Array
            }
            
            st.close(); // Cerrar query
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return recs;
    } 
    
    /**
     * Aprobar o desaprobar un recorrido
     * @param codigo Código del recorrido
     * @param ap Aprobada (true) o Desaprobada (false)
     */
    public static void saveAprobar(String codigo, boolean ap){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            Statement st_actualizarApCarrera = con.createStatement();
            String query2 = "UPDATE Recorrido SET aprobada="+((ap)? "1": "0")+" WHERE codigo='"+codigo+"'";  // Actualizar campo 'aprobada' del recorrido
            st_actualizarApCarrera.executeUpdate(query2);
            st_actualizarApCarrera.close(); // Cerrar query
            
            con.close(); // Cerrar conexión
            
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
    
    /**
     * Guardar un nuevo recorrido o actualizar uno ya existente
     * @param rec Recorrido a guardar
     * @return True si se guardó, false si hubo un error
     */
    public static boolean save(Recorrido rec){
        boolean saved = false;
        
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try {
            con.setAutoCommit(false); // Deshabilitar el AUTOCOMMIT de MYSQL
            
            Statement st = con.createStatement();
            
            /*
                Comprobar si existe el recorrido en la bd
            */
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Recorrido WHERE codigo='"+rec.getCodigo()+"'"); // Consultar número de recorridos en la BD con ese código
            
            int exists = 0;
            while (rs.next()) {
                exists = rs.getInt(1); // Cargar número
            }
            rs.close(); // Cerrar query
            
            if(exists==0){ //El recorrido no existe en la BD (Almacenar)
                String query = "INSERT INTO Recorrido (codigo, descripcion, kilometros)"
                + " values (?, ?, ?)";

                PreparedStatement preparedStmt = con.prepareStatement(query);
                preparedStmt.setString (1, rec.getCodigo());
                preparedStmt.setString (2, rec.getDescripcion());
                preparedStmt.setFloat(3, rec.getKilometros());

                preparedStmt.execute(); // Guardar datos del recorrido
                preparedStmt.close(); // Cerrar query
                
                if(!rec.getObservaciones().isEmpty()){ // Si el recorrido tiene observaciones
                    Statement st2 = con.createStatement();

                    String query2 = "INSERT INTO RecorridoObservacion (codigoRecorrido, observacion) VALUES ";

                    int i = 0;
                    for(String obs : rec.getObservaciones()){
                        if(i!=0)
                            query2+=",";
                        query2+="('"+rec.getCodigo()+"', '"+obs+"')";
                        i++;
                    }

                    st2.executeUpdate(query2); // Insertar observaciones en la BD
                    st2.close(); // Cerrar query
                }
            }else{ // La carrera ya existe en la BD (Actualizar)
                Statement st_actualizarDatosRecorrido = con.createStatement();
                String query2 = "UPDATE Recorrido SET descripcion='"+rec.getDescripcion()+"', kilometros="+rec.getKilometros()+" WHERE codigo='"+rec.getCodigo()+"'";
                st_actualizarDatosRecorrido.executeUpdate(query2); // Actualizar datos del recorrido
                st_actualizarDatosRecorrido.closeOnCompletion(); // Cerrar query
                
                Statement st_eliminarObservacionesPreviasRecorrido = con.createStatement();
                String query3 = "DELETE FROM RecorridoObservacion WHERE codigoRecorrido='"+rec.getCodigo()+"'";
                st_eliminarObservacionesPreviasRecorrido.executeUpdate(query3); // Eliminar todas las observaciones del recorrido
                st_eliminarObservacionesPreviasRecorrido.closeOnCompletion(); // Cerrar query
                
                if(!rec.getObservaciones().isEmpty()){ // Si el recorrido tiene observaciones
                    Statement st_insertarObservacionesRecorrido = con.createStatement();

                    String query4 = "INSERT INTO RecorridoObservacion (codigoRecorrido, observacion) VALUES ";

                    int i = 0;
                    for(String obs : rec.getObservaciones()){
                        if(i!=0)
                            query4+=",";
                        query4+="('"+rec.getCodigo()+"', '"+obs+"')";
                        i++;
                    }

                    st_insertarObservacionesRecorrido.executeUpdate(query4); // Insertar nuevas observaciones en la BD
                    st_insertarObservacionesRecorrido.closeOnCompletion(); // Cerrar query
                }
            }
            con.commit(); // Hacer commit de los querys
            saved=true; // Marcar como correcta
            
            con.setAutoCommit(true); // Habilitar el AUTOCOMMIT de MYSQL
            con.close(); // Cerrar conexión con la BD
        } catch (SQLException ex) {
            saved=false; // Marcar como fallida
            
            BDconnect.showMYSQLerrors(ex);  // Muestra los errores SQL
            
            try{
                if (con != null) { // Si no se ha cerrado la conexión
                    con.rollback(); // Hacer rollback de los querys
                    con.setAutoCommit(true); // Habilitar el AUTOCOMMIT de MYSQL
                    con.close(); // Cerrar conexión
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        System.out.println(rec.toString()); // Mostrar datos del recorrido en la terminal
        
        return saved;
    }
    
    /**
     * Elimina un recorrido dado
     * @param codigo Código del recorrido
     */
    public static void delete(String codigo){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try{
            Statement st_eliminarRecorrido = con.createStatement();
            String query = "DELETE FROM Recorrido WHERE codigo='"+codigo+"'";
            st_eliminarRecorrido.executeUpdate(query); // Eliminar recorrido
            st_eliminarRecorrido.close(); // Cerrar query
            
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); // Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // Cerrar conexión
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
}
