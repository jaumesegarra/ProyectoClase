/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Aportacion;
import models.AportacionDinero;
import models.Patrocinador;
import utils.BDconnect;

/**
 * Aportación Dinero DAO
 * @version 0.1
 * @see AportacionDinero
 * @author Jaume Segarra
 */
public class AportacionDineroDAO {
    
    /**
     * Cargar una aportación de tipo Dinero
     * @param id Id 
     * @return Datos de la aportación
     */
    public static AportacionDinero load(int id){
        return AportacionDineroDAO.load(id, null);
    }
    
    /**
     * Cargar una aportación de tipo Dinero
     * @param id Id 
     * @param con Conexión a la BD
     * @return Datos de la aportación
     */
    protected static AportacionDinero load(int id , Connection con){
        AportacionDinero ap = null;
        boolean new_con = true; // Si es una nueva conexión a la BD
        
        if(con !=  null)
            new_con = false;
        else //Si no se ha pasado ninguna conexión a la BD
            con = BDconnect.connect(); //se crea una nueva
        
        try{
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT don.*, dono.cantidad FROM Donacion don, DonacionDinero dono WHERE don.id="+id+" AND don.id=dono.idDonacion");
            
            if (rs.next())
            {
                Patrocinador aportador = PatrocinadorDAO.load(rs.getString("nombrePatrocinador"), con);
                
                ap = new AportacionDinero(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getFloat("cantidad"));
            }
            
            st.close();
            if(new_con) //Si era una nueva conexión
                con.close(); //se cierra
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return ap;
    }
    
    /**
     * Cargar todas las aportaciones de tipo dinero
     * @return Array con las aportaciones
     */
    public static ArrayList<Aportacion> loadAll(){
        ArrayList<Aportacion> al = new ArrayList<Aportacion>();
        Connection con = BDconnect.connect();
        
        try{
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT d.*, ddi.cantidad FROM DonacionDinero ddi, Donacion d WHERE ddi.idDonacion = d.id");
            
            while (rs.next())
            {
                Patrocinador aportador = PatrocinadorDAO.load(rs.getString("nombrePatrocinador"), con); //cargar datos del patrocinador
                
                al.add(new AportacionDinero(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getFloat("cantidad"))); //Añadir aportación al Array
            }
             
            rs.close();
            con.close();
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return al;
    }
    
    /**
     * Guardar una nueva aportación o modificar una existente
     * @param ap Aportacion a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Aportacion ap){
        Connection con = BDconnect.connect();
        boolean saved = AportacionDAO.save(ap,con);
        
        if(saved){
            try {
                Statement st = con.createStatement();

                /*
                    Comprobar si existe la aportación en la bd
                */
                ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM DonacionDinero WHERE idDonacion="+ap.getId());

                int exists = 0;
                while (rs.next()) {
                    exists = rs.getInt(1);
                }
                rs.close();

                if(exists==0){ //La aportación no existe en la BD (Almacenar)
                    String query = "INSERT INTO DonacionDinero (idDonacion, cantidad)"
                    + " values (?, ?)";

                    PreparedStatement preparedStmt = con.prepareStatement(query);
                    preparedStmt.setInt (1, ap.getId());
                    preparedStmt.setFloat(2, ((AportacionDinero)ap).getCantidad());

                    preparedStmt.execute();
                    preparedStmt.closeOnCompletion();
                    
                }else{
                    Statement st_actualizarDatosAportacion = con.createStatement();
                    String query2 = "UPDATE DonacionDinero SET cantidad= cantidad + "+((AportacionDinero)ap).getCantidad()+" WHERE idDonacion="+ap.getId();
                    st_actualizarDatosAportacion.executeUpdate(query2);
                    st_actualizarDatosAportacion.closeOnCompletion();
                    
                    /*
                        Obtener nueva cantidad de la aportación
                    */
                    Statement st_c = con.createStatement();
                    ResultSet rs_c = st_c.executeQuery("SELECT cantidad FROM DonacionDinero WHERE idDonacion="+ap.getId());

                    if (rs_c.next()) {
                        ((AportacionDinero)ap).setCantidad(rs_c.getFloat(1));
                    }
                    rs_c.close();
                }
                
                saved=true;
                con.commit();
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException ex) {
                saved=false;
                BDconnect.showMYSQLerrors(ex);
                
                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
            }
        }
        
        System.out.println(((AportacionDinero)ap).toString()); //Mostrar datos de la aportación
        
        return saved;
    }
}
