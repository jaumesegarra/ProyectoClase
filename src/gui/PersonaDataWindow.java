/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import models.Persona;

/**
 *
 * @author jaumesegarra
 */
public interface PersonaDataWindow {
    public abstract void loadFields(Persona pers);
    public abstract void resetFields();
    public abstract void noguardadoActionPermormed(java.awt.event.KeyEvent evt);
}
