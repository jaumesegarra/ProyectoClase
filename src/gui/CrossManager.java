/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

//import com.apple.eawt.Application;
import de.javasoft.plaf.synthetica.SyntheticaPlainLookAndFeel;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import utils.BDconnect;
import utils.Generar_Informes;

/** 
 *
 * @author jaumesegarra
 */
public class CrossManager extends javax.swing.JFrame {
  
    public CrossManager(){
        initComponents();
        jMenuBar1.setBackground(new Color(10, 151, 202));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">/*GEN-BEGIN:initComponents*/
    private void initComponents() {
        
       UIManager.put("Synthetica.window.decoration", Boolean.FALSE);
       
	try
	{
	  UIManager.setLookAndFeel(new SyntheticaPlainLookAndFeel());
	}
	catch (Exception e)
	{
	  e.printStackTrace();
	}

        setTitle("CrossManager v0.1");
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        Image image = new ImageIcon(this.getClass().getResource("images/logo.png")).getImage();
        setIconImage(image);
        this.setMaximumSize(new Dimension(1900, 1200));
        /*
        Application.getApplication().setDockIconImage(
            image);
        */
        Image imageBack = new ImageIcon(this.getClass().getResource("images/background.jpg")).getImage();

        escritorioPanel = new javax.swing.JDesktopPane(){
            protected void paintComponent(Graphics grphcs) {
                super.paintComponent(grphcs);
                grphcs.drawImage(imageBack, 0, 0, null);
            }

            @Override
            public Dimension getPreferredSize() {
                return new Dimension(imageBack.getWidth(null), imageBack.getHeight(null));
            }
        };
        
        jMenuBar1 = new javax.swing.JMenuBar();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        escritorioPanel.setMinimumSize(new java.awt.Dimension(800, 500));
        
        /*Menú principal*/
        javax.swing.JMenu principalMenu = new javax.swing.JMenu();
        principalMenu.setText("CrossManager");
        
        javax.swing.JMenuItem principalMenu_ConexionBD = new javax.swing.JMenuItem();
        principalMenu_ConexionBD.setText("Configuración de la BD");
        principalMenu_ConexionBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                principalMenu_ConexionBDActionPerformed(evt);
            }
        });
        principalMenu.add(principalMenu_ConexionBD);
        
        principalMenu.add(new JSeparator());
        
        javax.swing.JMenuItem principalMenu_Salir = new javax.swing.JMenuItem();
        principalMenu_Salir.setText("Salir");
        principalMenu_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                principalMenu_SalirActionPerformed(evt);
            }
        });
        principalMenu.add(principalMenu_Salir);
        
        
        jMenuBar1.add(principalMenu);
        /*Menú carrera*/
        javax.swing.JMenu carreraMenu = new javax.swing.JMenu();
        carreraMenu.setText("Carrera");

        
        carreraMenu_Nueva = new javax.swing.JMenuItem();
        carreraMenu_Nueva.setText("Nueva");
        carreraMenu_Nueva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_NuevaActionPerformed(evt);
            }
        });
        carreraMenu.add(carreraMenu_Nueva);

        javax.swing.JMenuItem carreraMenu_VerListado = new javax.swing.JMenuItem();
        carreraMenu_VerListado.setText("Ver Listado");
        carreraMenu_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_VerListadoActionPerformed(evt);
            }
        });
        carreraMenu.add(carreraMenu_VerListado);
        
        carreraMenu.add(new JSeparator());
        
        javax.swing.JMenu carreraMenu_regla = new javax.swing.JMenu();
        carreraMenu_regla.setText("Regla");
        carreraMenu.add(carreraMenu_regla);
        
        javax.swing.JMenuItem carreraMenu_regla_Nueva = new javax.swing.JMenuItem();
        carreraMenu_regla_Nueva.setText("Nueva");
        carreraMenu_regla_Nueva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_CrearReglaActionPerformed(evt);
            }
        });
        carreraMenu_regla.add(carreraMenu_regla_Nueva);
        
        javax.swing.JMenuItem carreraMenu_regla_VerListado = new javax.swing.JMenuItem();
        carreraMenu_regla_VerListado.setText("Ver Listado");
        carreraMenu_regla_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_VerReglasActionPerformed(evt);
            }
        });
        carreraMenu_regla.add(carreraMenu_regla_VerListado);
        
        javax.swing.JMenu carreraMenu_BolsaCorredor = new javax.swing.JMenu();
        carreraMenu_BolsaCorredor.setText("Bolsa Corredor");
        carreraMenu.add(carreraMenu_BolsaCorredor);
        
        javax.swing.JMenuItem carreraMenu_BolsaCorredor_AñadirRegalos = new javax.swing.JMenuItem();
        carreraMenu_BolsaCorredor_AñadirRegalos.setText("Añadir regalos");
        carreraMenu_BolsaCorredor_AñadirRegalos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_BolsaCorredor_AñadirRegalosActionPerformed(evt);
            }
        });
        carreraMenu_BolsaCorredor.add(carreraMenu_BolsaCorredor_AñadirRegalos);
        
        javax.swing.JMenuItem carreraMenu_BolsaCorredor_VerListadoBolsaCorredor = new javax.swing.JMenuItem();
        carreraMenu_BolsaCorredor_VerListadoBolsaCorredor.setText("Ver Listado");
        carreraMenu_BolsaCorredor_VerListadoBolsaCorredor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carreraMenu_BolsaCorredor_VerListadoBolsaCorredorActionPerformed(evt);
            }
        });
        carreraMenu_BolsaCorredor.add(carreraMenu_BolsaCorredor_VerListadoBolsaCorredor);
        
        
        jMenuBar1.add(carreraMenu);

        /* Menú recorrido*/
        javax.swing.JMenu recorridoMenu = new javax.swing.JMenu();
        recorridoMenu.setText("Recorrido");
        
        recorridoMenu_Nuevo = new javax.swing.JMenuItem();
        recorridoMenu_Nuevo.setText("Nuevo");
        recorridoMenu_Nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recorridoMenu_NuevoActionPerformed(evt);
            }
        });
        recorridoMenu.add(recorridoMenu_Nuevo);
        
        javax.swing.JMenuItem recorridoMenu_VerListado = new javax.swing.JMenuItem();
        recorridoMenu_VerListado.setText("Ver Listado");
        recorridoMenu_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recorridoMenu_VerListadoActionPerformed(evt);
            }
        });
        recorridoMenu.add(recorridoMenu_VerListado);
        
        jMenuBar1.add(recorridoMenu);
        
        javax.swing.JMenu informesMenu = new javax.swing.JMenu();
        informesMenu.setText("Informe");
        
        javax.swing.JMenu informesMenu_Generar = new javax.swing.JMenu();
        informesMenu_Generar.setText("Generar");
        
        javax.swing.JMenu informesMenu_GenerarPLI = new javax.swing.JMenu();
        informesMenu_GenerarPLI.setText("PLI");
        
        informesMenu_Generar.add(informesMenu_GenerarPLI);
        
        javax.swing.JMenuItem informesMenu_GenerarInTo = new javax.swing.JMenuItem();
        informesMenu_GenerarInTo.setText("Todos");
        informesMenu_GenerarInTo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInGeneral();
                Generar_Informes.generarInAyuntamiento();
            }
        });
        informesMenu_GenerarPLI.add(informesMenu_GenerarInTo);
        
        informesMenu_GenerarPLI.add(new JSeparator());
        
        javax.swing.JMenuItem informesMenu_GenerarInGe = new javax.swing.JMenuItem();
        informesMenu_GenerarInGe.setText("Informe general");
        informesMenu_GenerarInGe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInGeneral();
            }
        });
        informesMenu_GenerarPLI.add(informesMenu_GenerarInGe);
        
        javax.swing.JMenuItem informesMenu_GenerarInAP = new javax.swing.JMenuItem();
        informesMenu_GenerarInAP.setText("Informe para el Ayuntamiento");
        informesMenu_GenerarInAP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInAyuntamiento();
            }
        });
        informesMenu_GenerarPLI.add(informesMenu_GenerarInAP);
        
        javax.swing.JMenuItem informesMenu_GenerarInAportaciones = new javax.swing.JMenuItem();
        informesMenu_GenerarInAportaciones.setText("Informe de aportaciones");
        informesMenu_GenerarInAportaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInAportaciones();
            }
        });
        informesMenu_Generar.add(informesMenu_GenerarInAportaciones);
        
        javax.swing.JMenuItem informesMenu_GenerarInVoluntarios = new javax.swing.JMenuItem();
        informesMenu_GenerarInVoluntarios.setText("Informe de voluntarios");
        informesMenu_GenerarInVoluntarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInVoluntarios();
            }
        });
        informesMenu_Generar.add(informesMenu_GenerarInVoluntarios);
        
        
        javax.swing.JMenuItem informesMenu_GenerarInGanadores = new javax.swing.JMenuItem();
        informesMenu_GenerarInGanadores.setText("Informe de ganadores");
        informesMenu_GenerarInGanadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.generarInGanadores();
            }
        });
        informesMenu_Generar.add(informesMenu_GenerarInGanadores);
        
        informesMenu.add(informesMenu_Generar);
        
        javax.swing.JMenuItem informesMenu_VerCarpeta = new javax.swing.JMenuItem();
        informesMenu_VerCarpeta.setText("Ver Carpeta");
        informesMenu_VerCarpeta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Generar_Informes.verCarpeta();
            }
        });
        informesMenu.add(informesMenu_VerCarpeta);
        
        javax.swing.JMenu patrocinadorMenu = new javax.swing.JMenu();
        patrocinadorMenu.setText("Patrocinador");
        javax.swing.JMenuItem patrocinadorMenu_Nuevo = new javax.swing.JMenuItem();
        patrocinadorMenu_Nuevo.setText("Nuevo");
        patrocinadorMenu_Nuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patrocinadorMenu_NuevoActionPerformed(evt);
            }
        });
        patrocinadorMenu.add(patrocinadorMenu_Nuevo);
        
        javax.swing.JMenuItem patrocinadorMenu_VerListado = new javax.swing.JMenuItem();
        patrocinadorMenu_VerListado.setText("Ver Listado");
        patrocinadorMenu_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patrocinadorMenu_VerListadoActionPerformed(evt);
            }
        });
        patrocinadorMenu.add(patrocinadorMenu_VerListado);
        
        patrocinadorMenu.add(new JSeparator());
        
        javax.swing.JMenu patrocinadorMenu_Aportacion = new javax.swing.JMenu();
        patrocinadorMenu_Aportacion.setText("Aportación");
        
        patrocinadorMenu.add(patrocinadorMenu_Aportacion);
        
        javax.swing.JMenuItem patrocinadorMenu_AportacionAñadir = new javax.swing.JMenuItem();
        patrocinadorMenu_AportacionAñadir.setText("Añadir");
        patrocinadorMenu_AportacionAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patrocinadorMenu_AportacionAñadirActionPerformed(evt);
            }
        });
        patrocinadorMenu_Aportacion.add(patrocinadorMenu_AportacionAñadir);
        
        javax.swing.JMenuItem patrocinadorMenu_AportacionVerListado = new javax.swing.JMenuItem();
        patrocinadorMenu_AportacionVerListado.setText("Ver Listado");
        patrocinadorMenu_AportacionVerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                patrocinadorMenu_AportacionVerListadoActionPerformed(evt);
            }
        });
        patrocinadorMenu_Aportacion.add(patrocinadorMenu_AportacionVerListado);
        
        jMenuBar1.add(patrocinadorMenu);
        
        javax.swing.JMenu corredorMenu = new javax.swing.JMenu();
        corredorMenu.setText("Corredor");
        
        javax.swing.JMenuItem corredorMenu_Inscribir = new javax.swing.JMenuItem();
        corredorMenu_Inscribir.setText("Inscribir");
        corredorMenu_Inscribir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corredorMenu_InscribirActionPerformed(evt);
            }
        });
        corredorMenu.add(corredorMenu_Inscribir);
        
        javax.swing.JMenuItem corredorMenu_VerListado = new javax.swing.JMenuItem();
        corredorMenu_VerListado.setText("Ver Listado");
        corredorMenu_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corredorMenu_VerListadoActionPerformed(evt);
            }
        });
        corredorMenu.add(corredorMenu_VerListado);
        
        javax.swing.JMenuItem corredorMenu_RetirarDorsal = new javax.swing.JMenuItem();
        corredorMenu_RetirarDorsal.setText("Retirar Dorsal");
        corredorMenu_RetirarDorsal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corredorMenu_RetirarDorsalActionPerformed(evt);
            }
        });
        corredorMenu.add(corredorMenu_RetirarDorsal);
        
        corredorMenu.add(new JSeparator());
        
        javax.swing.JMenu corredorMenu_Club = new javax.swing.JMenu();
        corredorMenu_Club.setText("Club");
        
        javax.swing.JMenuItem corredorMenu_Club_Añadir = new javax.swing.JMenuItem();
        corredorMenu_Club_Añadir.setText("Añadir");
        corredorMenu_Club_Añadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corredorMenu_Club_AñadirActionPerformed(evt);
            }
        });
        corredorMenu_Club.add(corredorMenu_Club_Añadir);
        
        javax.swing.JMenuItem corredorMenu_Club_VerListado = new javax.swing.JMenuItem();
        corredorMenu_Club_VerListado.setText("Ver Listado");
        corredorMenu_Club_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corredorMenu_Club_VerListadoActionPerformed(evt);
            }
        });
        corredorMenu_Club.add(corredorMenu_Club_VerListado);
                
        corredorMenu.add(corredorMenu_Club);
        
        jMenuBar1.add(corredorMenu);
        
        javax.swing.JMenu voluntarioMenu = new javax.swing.JMenu();
        voluntarioMenu.setText("Voluntario");
        
        javax.swing.JMenuItem voluntarioMenu_Inscribir = new javax.swing.JMenuItem();
        voluntarioMenu_Inscribir.setText("Inscribir");
        voluntarioMenu_Inscribir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voluntarioMenu_InscribirActionPerformed(evt);
            }
        });
        voluntarioMenu.add(voluntarioMenu_Inscribir);
        
        javax.swing.JMenuItem voluntarioMenu_VerListado = new javax.swing.JMenuItem();
        voluntarioMenu_VerListado.setText("Ver Listado");
        voluntarioMenu_VerListado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                voluntarioMenu_VerListadoActionPerformed(evt);
            }
        });
        voluntarioMenu.add(voluntarioMenu_VerListado);
        
        jMenuBar1.add(voluntarioMenu);
        
        jMenuBar1.add(informesMenu);
        
        ayudaMenu = new javax.swing.JMenu();
        ayudaMenu.setText("Ayuda");
        
        javax.swing.JMenuItem ayudaMenu_Manual = new javax.swing.JMenuItem();
        ayudaMenu_Manual.setText("Consultar manual");
        ayudaMenu_Manual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                try{
                    Desktop.getDesktop().browse(new File("html_manual/index.html").toURI());
                }catch(IOException e){}
            }
        });
        ayudaMenu_Manual.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
        ayudaMenu.add(ayudaMenu_Manual);
        
        javax.swing.JMenuItem ayudaMenu_AcercaDe = new javax.swing.JMenuItem();
        ayudaMenu_AcercaDe.setText("Acerca de");
        ayudaMenu_AcercaDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ayudaMenu_AcercaDeActionPerformed(evt);
            }
        });
        ayudaMenu_AcercaDe.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0));
        
        ayudaMenu.add(ayudaMenu_AcercaDe);
        
        jMenuBar1.add(ayudaMenu);
        setJMenuBar(jMenuBar1);
                
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorioPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(escritorioPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>/*GEN-END:initComponents*/

    private void principalMenu_ConexionBDActionPerformed(java.awt.event.ActionEvent evt) {
        new ConfigBDJDialog().setVisible(true);
    }
            
    private void principalMenu_SalirActionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
    }
    
    private void carreraMenu_NuevaActionPerformed(java.awt.event.ActionEvent evt) {
        CarreraWindow ccWindow = new CarreraWindow(escritorioPanel);
        ccWindow.setVisible(true);
        escritorioPanel.add(ccWindow);
        try{
        ccWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void carreraMenu_VerListadoActionPerformed(java.awt.event.ActionEvent evt) {
        ListadoCarrerasWindow ccWindow = new ListadoCarrerasWindow(escritorioPanel);
        ccWindow.setVisible(true);
        escritorioPanel.add(ccWindow);
        try{
        ccWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    
    private void carreraMenu_CrearReglaActionPerformed(java.awt.event.ActionEvent evt) {
        ReglaWindow crWindow = new ReglaWindow(escritorioPanel);
        crWindow.setVisible(true);
        escritorioPanel.add(crWindow);
        try{
        crWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void carreraMenu_VerReglasActionPerformed(java.awt.event.ActionEvent evt) {
        ListadoReglasWindow crWindow = new ListadoReglasWindow(escritorioPanel);
        crWindow.setVisible(true);
        escritorioPanel.add(crWindow);
        try{
        crWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void carreraMenu_BolsaCorredor_AñadirRegalosActionPerformed(ActionEvent evt) {
        GrupoRegaloWindow cgprWindow = new GrupoRegaloWindow(escritorioPanel);
        cgprWindow.setVisible(true);
        escritorioPanel.add(cgprWindow);
        try{
        cgprWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void carreraMenu_BolsaCorredor_VerListadoBolsaCorredorActionPerformed(ActionEvent evt) {
        ListadoBolsaCorredorWindow cgprWindow = new ListadoBolsaCorredorWindow(escritorioPanel);
        cgprWindow.setVisible(true);
        escritorioPanel.add(cgprWindow);
        try{
        cgprWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
        
    }
    
    private void recorridoMenu_NuevoActionPerformed(java.awt.event.ActionEvent evt) {
        RecorridoWindow crWindow = new RecorridoWindow(escritorioPanel);
        crWindow.setVisible(true);
        escritorioPanel.add(crWindow);
        try{
        crWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void recorridoMenu_VerListadoActionPerformed(java.awt.event.ActionEvent evt) {
        ListadoRecorridosWindow ccWindow = new ListadoRecorridosWindow(escritorioPanel);
        ccWindow.setVisible(true);
        escritorioPanel.add(ccWindow);
        try{
        ccWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void corredorMenu_InscribirActionPerformed(ActionEvent evt) {
        CorredorWindow cpWindow = new CorredorWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void corredorMenu_RetirarDorsalActionPerformed(ActionEvent evt) {
        RetirarDorsalCorredorWindow cpWindow = new RetirarDorsalCorredorWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void corredorMenu_VerListadoActionPerformed(ActionEvent evt) {
        ListadoCorredorWindow cpWindow = new ListadoCorredorWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void corredorMenu_Club_AñadirActionPerformed(ActionEvent evt) {
        ClubWindow cpWindow = new ClubWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void corredorMenu_Club_VerListadoActionPerformed(ActionEvent evt) {
        ListadoClubsWindow cpWindow = new ListadoClubsWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void voluntarioMenu_InscribirActionPerformed(ActionEvent evt) {
        VoluntarioWindow cpWindow = new VoluntarioWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void voluntarioMenu_VerListadoActionPerformed(ActionEvent evt) {
        ListadoVoluntariosWindow cpWindow = new ListadoVoluntariosWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void patrocinadorMenu_NuevoActionPerformed(java.awt.event.ActionEvent evt){
        PatrocinadorWindow cpWindow = new PatrocinadorWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void patrocinadorMenu_VerListadoActionPerformed(java.awt.event.ActionEvent evt){
        ListadoPatrocinadoresWindow cpWindow = new ListadoPatrocinadoresWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void patrocinadorMenu_AportacionAñadirActionPerformed(java.awt.event.ActionEvent evt){
        AportacionWindow cpWindow = new AportacionWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void patrocinadorMenu_AportacionVerListadoActionPerformed(java.awt.event.ActionEvent evt){
        ListadoAportacionesWindow cpWindow = new ListadoAportacionesWindow(escritorioPanel);
        cpWindow.setVisible(true);
        escritorioPanel.add(cpWindow);
        try{
        cpWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    public void ayudaMenu_AcercaDeActionPerformed(java.awt.event.ActionEvent evt){
        new AcercaDeJDialog().setVisible(true);
    }
    
    public void closeAllWindowsExcept(JInternalFrame f){
        JInternalFrame[] frames = escritorioPanel.getAllFrames();
        for (int i = 0; i < frames.length; i++)
             if(!frames[i].getClass().equals(f))
                 frames[i].dispose();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        try {
            
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CrossManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CrossManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CrossManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CrossManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CrossManager().setVisible(true);
                
                if(!BDconnect.configExists())
                    new ConfigBDJDialog(true).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane escritorioPanel;
    private javax.swing.JMenuBar jMenuBar1;
    
    private javax.swing.JMenuItem principalMenu_FinalizarPlI;
    
    private javax.swing.JMenuItem carreraMenu_Nueva;
    
    private javax.swing.JMenuItem recorridoMenu_Nuevo;
    
    private javax.swing.JMenu ayudaMenu;
    // End of variables declaration//GEN-END:variables
}
