/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import dao.CarreraDAO;
import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import models.Carrera;

/**
 *
 * @author jaumesegarra
 */
public class ListadoCarrerasWindow extends ListadoWindow{
    
    private javax.swing.JDesktopPane desc= null;
    private ArrayList<Carrera> carreras = CarreraDAO.loadAll();
    private boolean ret=false;
    /**
     * Creates new form CrearCarreraWindow
     */
    public ListadoCarrerasWindow(javax.swing.JDesktopPane desc) {
        add(new Panel());
        this.desc = desc;
        
        initComponents();
        setTitle("Listado de carreras");
        
        cargarCarreras();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">/*GEN-BEGIN:initComponents*/
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        carrerasTable = new javax.swing.JTable();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(true);
        setMinimumSize(new java.awt.Dimension(600, 150));
        setPreferredSize(new java.awt.Dimension(600, 400));
        setVisible(true);
        
            carrerasTable.setModel(new javax.swing.table.DefaultTableModel(
                    new Object [][] {

                    },
                    new Object [] {
                        "Categoría", "Sexo", "Hora comienzo", "Recorrido", "Aprobada", "*", "**"
                    }
                ) {
                    Class[] types = new Class [] {
                        java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
                    };
                    boolean[] canEdit = new boolean [] {
                        false, false, false, false,true, true, true
                    };

                    public Class getColumnClass(int columnIndex) {
                        return types [columnIndex];
                    }

                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit [columnIndex];
                    }
                });
                carrerasTable.getColumn("*").setCellRenderer(new ButtonRenderer());
                carrerasTable.getColumn("*").setCellEditor(new ButtonEditor(new JCheckBox(), this, true));
                carrerasTable.getColumn("**").setCellRenderer(new ButtonRenderer());
                carrerasTable.getColumn("**").setCellEditor(new ButtonEditor(new JCheckBox(), this, true));
            
                carrerasTable.getModel().addTableModelListener(new TableModelListener() {
                    public void tableChanged(TableModelEvent e) {
                        if(e.getColumn() == 4 && !ret){
                            String categoria = (String)carrerasTable.getValueAt(e.getFirstRow(), 0);
                            String sexo = (String)carrerasTable.getValueAt(e.getFirstRow(), 1);
                            
                            Boolean apChange = (Boolean)carrerasTable.getValueAt(e.getFirstRow(), 4);
                            Boolean apAnt = (apChange) ? false : true;
                                                        
                            if((apChange && carreras.get(e.getFirstRow()).getRecorrido().isAprobada()) || !apChange){ // Comprueba si cuando va a aprobar el recorrido de esa carrera está aprobado
                                int reply = JOptionPane.showConfirmDialog(null, "Quieres "+((apChange)? "aprobar" : "desaprobar")+" la carrera '"+categoria+" ("+sexo+")'?", "Estás seguro?", JOptionPane.YES_NO_OPTION);
                                if (reply == JOptionPane.YES_OPTION){
                                    CarreraDAO.saveAprobar(categoria,sexo,apChange);
                                    carrerasTable.setValueAt(((apChange)? "detalles" : "editar"), e.getFirstRow(), 5);
                                    carrerasTable.setValueAt(((apChange)? "--" : "eliminar"), e.getFirstRow(), 6);
                                    JInternalFrame[] frames = desc.getAllFrames();
                                    for (int i = 0; i < frames.length; i++)
                                        if(frames[i].getTitle().contains(categoria) && frames[i].getTitle().contains(sexo))
                                            frames[i].dispose();
                                }else{
                                    ret=true;
                                    carrerasTable.setValueAt(apAnt, e.getFirstRow(), 4);
                                    ret=false;
                                }
                            }else{
                                JOptionPane.showMessageDialog(null, "El recorrido de esta carrera no esta aprobado!", "Error", JOptionPane.ERROR_MESSAGE);
                                ret=true;
                                carrerasTable.setValueAt(apAnt, e.getFirstRow(), 4);
                                ret=false;
                            }
                        }
                    }
                });
        
        carrerasTable.getColumnModel().getColumn(5).setMinWidth(90);
        carrerasTable.getColumnModel().getColumn(6).setMinWidth(90);
        carrerasTable.getColumnModel().getColumn(5).setMaxWidth(90);
        carrerasTable.getColumnModel().getColumn(6).setMaxWidth(90);
        carrerasTable.getTableHeader().setReorderingAllowed(false);

        jScrollPane1.setViewportView(carrerasTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1))
//                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>/*GEN-END:initComponents*/
    
    private void cargarCarreras(){
        DefaultTableModel model = (DefaultTableModel) carrerasTable.getModel();
        for(Carrera car : this.carreras)
            model.addRow(obtainRow(car));
    }
    
    private Object[] obtainRow(Carrera car){
        Object[] o = null;
        
        o = new Object[]{car.getCategoria(),car.getSexo(),car.getHoraComienzo(), car.getRecorrido().getCodigo(),car.isAprobada(),((car.isAprobada()) ? "detalles" : "editar"), ((car.isAprobada()) ? "---" : "eliminar")};

        return o;
    }
    
    public void recargarCarreras(){ //No funciona
        
        DefaultTableModel model = (DefaultTableModel) carrerasTable.getModel();
        
        if (carrerasTable.isEditing()) {
            carrerasTable.getCellEditor().cancelCellEditing();
        }
        
        for (int i = carrerasTable.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        
        carrerasTable.revalidate();
        carrerasTable.removeAll();
        cargarCarreras();
    }
    
    
    public void ButtonTableActionEvent(ButtonEditor b){
        String categoria = (String)carrerasTable.getValueAt(b.getSelectedRow(), 0),
               sexo = (String)carrerasTable.getValueAt(b.getSelectedRow(), 1),
               labelButton = b.getLabel();
        
        switch(labelButton){
            case "editar":
                editarButtonTableActionEvent(categoria,sexo, true);
                break;
            case "eliminar":
                eliminarButtonTableActionEvent(categoria,sexo, b);
                break;
            case "detalles":
                editarButtonTableActionEvent(categoria,sexo, false);
                break;
        }
    }
    
    private void editarButtonTableActionEvent(String categoria, String sexo, boolean editable){
        CarreraWindow ccWindow = new CarreraWindow(desc, categoria, sexo);
        ccWindow.setVisible(true);
        if(!editable)
            ccWindow.setNon_Editable(true);
        desc.add(ccWindow);
        try{
        ccWindow.setSelected(true);
        }catch(java.beans.PropertyVetoException e){}
    }
    
    private void eliminarButtonTableActionEvent(String categoria, String sexo, ButtonEditor b){
        int reply = JOptionPane.showConfirmDialog(null, "Quieres eliminar la carrera '"+categoria+" ("+sexo+")'? \nNo podrás recuperar sus datos posteriormente.", "Estás seguro?", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION){
                    CarreraDAO.delete(categoria,sexo);
                    b.setDeleted();
                }
    }
    
    private class Panel extends JPanel {
        
        public Dimension getPreferredSize(){
            return new Dimension(300, 300);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ListadoCarrerasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ListadoCarrerasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ListadoCarrerasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ListadoCarrerasWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable carrerasTable;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}