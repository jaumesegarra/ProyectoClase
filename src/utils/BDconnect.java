/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.JOptionPane;

/**
 * Funciones para la conexión a la BD
 * @version 0.2
 * @author Jaume Segarra
 */
public class BDconnect {

    private static final File CONFIG_FILE = new File("config_db"); //Fichero de configuración
    private static String key = "92AE31A79FEEB2A3", 
            iv = "0123456789ABCDEF"; //Llaves de encriptado del fichero

    /**
     * Crea la url jdbc a partir de los datos pasados
     * @param host Host
     * @param port Puerto
     * @param database Nombre de la base de datos (No obligatorio)
     * @param username Usuario
     * @param password Contraseña
     * @return jdbc url
     */
    private static String obtainJDBC_url(String host, int port, String database, String username, String password) {
        return "jdbc:mysql://" + host + ":" + port + ((database != null) ? "/" + database : "") + "?user=" + username + "&password=" + password;
    }
    
    /**
     * Comprueba si existe el fichero de configuración
     * @return True si existe, False si no existe
     */
    public static boolean configExists(){
        boolean res = false;
        if(CONFIG_FILE.exists())
            res = true;
        return res;
    }
    
    /**
     * Comprobar una conexión a la BD
     * @param host Host
     * @param port Puerto
     * @param database Nombre de la BD
     * @param username Usuario
     * @param password Contraseña
     * @return True si conecta, False si hubo un error
     */
    public static boolean testConnection(String host, int port, String database, String username, String password) {
        boolean result = false;
        String url = obtainJDBC_url(host, port, database, username, password); //obtener url jdbc

        try {
            Connection con = BDconnect.connect(url); //intentar conectar
            if (con != null){
                result = true; //conecta
                con.close();
            }
        
        } catch (SQLException e) {
            result=false;
        }

        return result;
    }

    /**
     * escribir fichero de configuración a partir de los datos pasados
     * @param host Host
     * @param port Puerto
     * @param database Nombre de la BD
     * @param username Usuario
     * @param password Contraseña
     * @return True si se escribió correctamente, False si no
     */
    public static boolean writeConfig(String host, int port, String database, String username, String password) {
        boolean saved = false;
        try {

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivParameterSpec);

            String c = host + "#_#" + port + "#_#" + database + "#_#" + username + "#_#" + password; //codificar datos separandolos con #_#

            byte[] encrypted = cipher.doFinal(c.getBytes()); //encriptar
            String textEnc = new String(Base64.getEncoder().encode(encrypted)); //pasar a BASE64

            FileWriter fw = new FileWriter(CONFIG_FILE);
            fw.write(textEnc); //Guardar datos encriptados en el fichero
            fw.close();

            saved = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saved;
    }

    /**
     * Leer fichero de configuración
     * @return Array de String con los datos de conexión
     */
    public static String[] readConfig() {
        String[] config = null;

        if (CONFIG_FILE.exists()) { //Si existe el ficherod econfiguración
            try {
                FileReader fr = new FileReader(CONFIG_FILE);
                BufferedReader br = new BufferedReader(fr);

                String line;
                while ((line = br.readLine()) != null) {
                    try {
                        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
                        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
                        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());

                        byte[] enc = Base64.getDecoder().decode(line); //Decodifica el BASE64
                        cipher.init(Cipher.DECRYPT_MODE, skeySpec, ivParameterSpec); 
                        byte[] decrypted = cipher.doFinal(enc); // Desencripta la linea

                        line = new String(decrypted);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    config = line.split("#_#"); //Trocea la linea con el separador #_#
                }

                br.close();
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return config;
    }

    /**
     * Crea la estructura de la BD
     * @param host Host
     * @param port Puerto
     * @param database Nombre de la base dde datos
     * @param username Usuario
     * @param password Contraseña
     * @return True si se creó, False si hubo un error
     */
    public static boolean createStructure(String host, int port, String database, String username, String password) {
        boolean result = false;

        Connection con = BDconnect.connect(obtainJDBC_url(host, port, database, username, password)); //Conecta con la BD pasada

        try {
            Statement st = con.createStatement();
            result = executeScript("estructura.sql", st); //executa el fichero sql
            st.close();
            con.close();

        } catch (SQLException e) {
            BDconnect.showMYSQLerrors(e);
            result = false;

            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
            }
        }

        return result;
    }

    /**
     * Ejecuta un conjunto de instrucciones de un fichero sql
     * @param path Fichero sql
     * @param st Statement MYSQL
     * @return True si se ejecuto correctamente
     * @throws SQLException 
     */
    private static boolean executeScript(String path, Statement st) throws SQLException {
        boolean isScriptExecuted = false;

        try {
            
            /*
                Leer fichero
            */
            BufferedReader in = new BufferedReader(new FileReader(path));
            String str;
            StringBuffer sb = new StringBuffer(); //Guarda las lineas del fichero aqui

            while ((str = in.readLine()) != null)
                sb.append(str.replace("    ", "").replace("﻿CREATE", "CREATE").replace("\t", "")); //Quita los espacios en blanco y los tabs
            
            in.close();
            
            
            /*
                Ejecutar las instrucciones 
            */
            String[] inst = sb.toString().split(";"); //Trocea el codigo por instrocción
            
            for (int i = 0; i < inst.length; i++)  //Recorre las instrucciones
                if (!inst[i].trim().equals(""))  //Si la instruccion no esta vacia
                    st.executeUpdate(inst[i]); // la ejecuta
            

            isScriptExecuted = true;
        } catch (Exception e) {
            System.err.println("Failed to Execute " + path + ". The error is " + e.getMessage());
        }

        return isScriptExecuted;
    }

    /**
     * Conectar a la base de datos guardada
     * @return Conexión a la BD
     */
    public static Connection connect() {
        String jdbc = "";

        String[] conf = readConfig(); // Lee la configuración de la BD
        if (conf != null) {
            jdbc = obtainJDBC_url(conf[0], new Integer(conf[1]), conf[2], conf[3], conf[4]); //Obtiene la URL jdbc
        }

        return BDconnect.connect(jdbc);
    }

    
    /**
     * Conecta a una base de datos pasando su jdbc
     * @param conData Enlace jdbc
     * @return Connexión a la BD, Null si hubo un error
     */
    public static Connection connect(String conData) {
        Connection conn = null;

        if (!conData.trim().isEmpty()) {
            try {
                conn = DriverManager.getConnection(conData);

            } catch (SQLException ex) {
                if (ex.getSQLState().equals("08S01")) {
                    JOptionPane.showMessageDialog(null, "Imposible conectar con la BD", "MYSQL Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    showMYSQLerrors(ex);
                }
            }
        }

        return conn;
    }

    /**
     * Muestra los errores SQL
     * @param ex SQLException
     */
    public static void showMYSQLerrors(SQLException ex) {
        JOptionPane.showMessageDialog(null, ex.getMessage(), "MYSQL Error " + ex.getSQLState(), JOptionPane.ERROR_MESSAGE);

        ex.printStackTrace();
        //System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());
    }
}
