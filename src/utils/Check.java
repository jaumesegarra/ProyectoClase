/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Utilidades para la comprobación de valores
 * @author Jaume Segarra
 */
public class Check {
    
    /**
     * Comprobar si un String es un número entero
     * @param text String con el número
     * @return True si es un número, False si no
     */
    public static boolean isNumber(String text){
        boolean result =false;
        try{
            Integer.parseInt(text);
            result=true;
        }catch(java.lang.NumberFormatException e){}
        
        return result;
    }
    
    /**
     * Comprobar si dos fechas son iguales (Sin contar horas, minutos y segundos)
     * @param date1 Fecha a comprobar
     * @param date2 Fecha a comprobar
     * @return True si son iguales
     */
    public static boolean isEq(GregorianCalendar date1, GregorianCalendar date2){
        boolean result = false;
        if(date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) && date1.get(Calendar.DAY_OF_YEAR) == date2.get(Calendar.DAY_OF_YEAR))
            result = true;
        return result;
    }
    
    /**
     * Comprobar si una fecha es mayor a otra (Sin contar horas, minutos y segundos)
     * @param date1 Fecha mayor
     * @param date2 Fecha menor
     * @return True si es mayor
     */
    public static boolean isGt(GregorianCalendar date1, GregorianCalendar date2){
        boolean result = false;
        if(date1.get(Calendar.YEAR) > date2.get(Calendar.YEAR) || ( date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR) && date1.get(Calendar.DAY_OF_YEAR) > date2.get(Calendar.DAY_OF_YEAR)))
            result = true;
        return result;
    }
}
