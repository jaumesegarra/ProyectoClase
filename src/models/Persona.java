/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author Jaume Segarra
 */
public class Persona {
    private String dni;
    private String nombre;
    private String apellidos;
    private Date fecha_nacimiento;
    private String direccion;
    private int telefono;
    private String sexo;
    
    public Persona(String dni, String nombre, String apellidos,Date fecha_nacimiento, String direccion, int telefono, String sexo){
        this.dni=dni;
        this.nombre=nombre;
        this.apellidos=apellidos;
        this.fecha_nacimiento=fecha_nacimiento;
        this.direccion=direccion;
        this.telefono=telefono;
        this.sexo=sexo;
    }

    public String getDni() {
        return dni;
    }

    public String getNombre(){
        return nombre;
    }
    
    public String getApellidos() {
        return apellidos;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    
    public String getDireccion(){
        return direccion;
    }
    
    public int getTelefono(){
        return telefono;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public String toString(){
        String cadena = "";
        cadena += "Dni: "+this.dni+"\n";
        cadena += "Nombre: "+this.nombre+"\n";
        cadena += "Apellidos: "+this.apellidos+"\n";
        cadena += "Sexo: "+this.sexo+"\n";
        cadena += "Fecha nacimiento: "+this.fecha_nacimiento+"\n";
        cadena += "Dirección: "+this.direccion+"\n";
        cadena += "Teléfono: "+this.telefono+"\n";
        return cadena;
    }
}
