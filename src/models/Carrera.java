/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.GregorianCalendar;

/**
 *
 * @author jaumesegarra
 */
public class Carrera {
    private GregorianCalendar fecha = new GregorianCalendar(2017,9,12);
    private String hora_comienzo;
    private ArrayList<Regla> reglas;
    private Recorrido recorrido;
    private String categoria;
    private String sexo;
    private boolean aprobada;
    
    public Carrera(String hora_comienzo, ArrayList<Regla> reglas, Recorrido recorrido, String categoria, String sexo){
        this.hora_comienzo=hora_comienzo;
        this.reglas=reglas;
        this.recorrido=recorrido;
        this.categoria=categoria;
        this.sexo=sexo;        
    }

    public GregorianCalendar getFecha() {
        return fecha;
    }

    public String getHora_comienzo() {
        return hora_comienzo;
    }
    
    public String getCategoria(){
        return categoria;
    }
    
    public String getSexo(){
        return sexo;
    }
    
    public String getHoraComienzo(){
        return hora_comienzo;
    }
    
    public Recorrido getRecorrido(){
        return recorrido;
    }
    
    public ArrayList<Regla> getReglas(){
        return reglas;
    }
    public boolean isAprobada(){
        return aprobada;
    }
    
    public void setAprobada(int a){
        this.aprobada = (a==1);
    }
    
    public static int[] getEdadParticipar(String categoria, String sexo){
        int[] edad = new int[2];
        switch(categoria){
            case "Benjamín":
                edad[0]=9;
                edad[1]=10;
                break;
            case "Alevín":
                edad[0]=11;
                edad[1]=12;
                break;
            case "Infantil":
                edad[0]=13;
                edad[1]=14;
                break;
            case "Cadete":
                edad[0]=15;
                edad[1]=16;
                break;
            case "Juvenil":
                edad[0]=17;
                edad[1]=19;
                break;
            case "Senior":
                edad[0]=20;
                edad[1]= (sexo.equals("HOMBRE")) ? 39: 34;
                break;
            case "Veterano":
                edad[0]=(sexo.equals("HOMBRE")) ? 40: 35;
                edad[1]= 99;
            case "minibenjamines":
                edad[0]=5;
                edad[1]=8;
                break;
            case "chupetines":
                edad[0]=0;
                edad[1]=4;
                break;
        }
        return edad;
    }
    
    public String toString(){
        String t= "";
        t+="Hora comienzo: "+this.getHoraComienzo()+"\n";
        t+="Reglas: \n";
        for(Regla r : this.reglas)
            t+="\t- "+r.getNombre()+" ("+r.getDescripcion()+")\n";
        t+= "Recorrido: "+this.recorrido.getCodigo()+"\n";
        t+= "Categoria: "+this.getCategoria()+"\n";
        t+= "Sexo: "+this.getSexo()+"\n";
        return t;
    }
}
