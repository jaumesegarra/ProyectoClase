/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import utils.BDconnect;

/**
 *
 * @author jaumesegarra
 */
public class Regalo {
    private int id;
    private AportacionObjeto aportacion;
    private float periodo_actual;
    private int cantidad;
    private int cantidad_disponible;
    private int cantidad_x_participante;
    
    public Regalo(int id, AportacionObjeto aportacion, float periodo_actual, int cantidad, int cantidad_disponible, int cantidad_x_participante){
        this.id=id;
        this.aportacion=aportacion;
        this.periodo_actual=periodo_actual;
        this.cantidad=cantidad;
        this.cantidad_disponible=cantidad_disponible;
        this.cantidad_x_participante=cantidad_x_participante;
    }
    
    public void setId(int id){
        this.id=id;
    }
    
    public int getId(){
        return id;
    }
    
    public AportacionObjeto getAportacion(){
        return aportacion;
    }
    
    public float getPeriodoActual(){
        return periodo_actual;
    }
    
    public int getCantidad(){
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public int getCantidadDisponible(){
        return cantidad_disponible;
    }

    public void setCantidadDisponible(int cantidad_disponible) {
        this.cantidad_disponible = cantidad_disponible;
    }
    
    public int getCantidadPorParticipante(){
        return cantidad_x_participante;
    }
    
    public void setPeriodoActual(float periodo_actual){
        this.periodo_actual = periodo_actual;
    }
    
    public String toString(){
        String ts= "";
        ts+="\tid: "+this.id+"\n";
        ts+="\tidDonacion: "+this.aportacion.getId()+"\n";
        ts+="\tcantidad: "+this.cantidad+"\n";
        ts+="\tcantidad_disponible: "+this.cantidad_disponible+"\n";
        ts+="\tperiodo_actual: "+this.periodo_actual+"\n";
        return ts;
    }
}
