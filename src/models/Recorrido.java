/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Jaume Segarra
 */
public class Recorrido {
    private String codigo;
    private String descripcion;
    private float kilometros;
    private ArrayList<String> observaciones;
    private boolean aprobada;
        
    public Recorrido(String codigo, String descripcion, float kilometros, ArrayList<String> observaciones){
        this.codigo=codigo;
        this.descripcion=descripcion;
        this.kilometros=kilometros;
        this.observaciones=observaciones;
    }
    
    public String getCodigo(){
        return codigo;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public float getKilometros(){
        return kilometros;
    }
    
    public ArrayList<String> getObservaciones(){
        return observaciones;
    }
    
    public boolean isAprobada(){
        return aprobada;
    }
    
    public void setAprobada(int a){
        this.aprobada = (a==1) ? true : false;
    }
    
    public String toString(){
        String t= "";
        t+="Codigo: "+this.codigo+"\n";
        t+="Descripción: "+this.descripcion+"\n";
        t+= "Kilometros: "+this.kilometros+"\n";
        t+="Observaciones: \n";
        for(String ob : this.observaciones)
            t+="\t- "+ob+"\n";
        return t;
    }
}
