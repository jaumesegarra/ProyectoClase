/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class AportacionDinero extends Aportacion{
    private float cantidad;
    
    public AportacionDinero(int id, String concepto, Patrocinador aportador, float cantidad){
        super(id, concepto, aportador);
        this.cantidad=cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    public float getCantidad(){
        return cantidad;
    }
    
    public String toString(){
        String t= super.toString();
        t+="Tipo: Dinero\n";
        t+="Cantidad: "+this.cantidad+"\n";
        
        return t;
    }
}
