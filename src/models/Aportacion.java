/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class Aportacion {
    private int id;
    private String concepto;
    private Patrocinador aportador;
    
    public Aportacion(int id, String concepto, Patrocinador aportador){
        this.id=id;
        this.concepto = concepto;
        this.aportador = aportador;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    public String getConcepto(){
        return concepto;
    }
    
    public Patrocinador getAportador(){
        return aportador;
    }
    
    public String toString(){
        String t= "";
        t+="Concepto: "+this.concepto+"\n";
        t+="Patrocinador: "+this.aportador.getNombre()+"\n";
        
        return t;
    }
}
