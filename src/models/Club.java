/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class Club {
    private int id;
    private String nombre;
    private String direccion;
    private int telefono;
    private String nombre_responsable;
    private int pos_total_corredores;
    
    public Club(int id, String nombre, String direccion, int telefono, String nombre_responsable) {
        this.id=id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.nombre_responsable = nombre_responsable;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Club other = (Club) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getNombre_responsable() {
        return nombre_responsable;
    }

    public void setNombre_responsable(String nombre_responsable) {
        this.nombre_responsable = nombre_responsable;
    }

    public int getPos_total_corredores() {
        return pos_total_corredores;
    }

    public void setPos_total_corredores(int pos_total_corredores) {
        this.pos_total_corredores = pos_total_corredores;
    }
    
    
    public String toString(){
        String cadena = "";
        cadena+="Id: "+this.id+"\n";
        cadena+="Nombre: "+this.nombre+"\n";
        cadena+="Dirección: "+this.direccion+"\n";
        cadena+="Responsable: "+this.nombre_responsable+"\n";
        return cadena;
    }
}
