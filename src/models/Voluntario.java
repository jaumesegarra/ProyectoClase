/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;



import java.util.ArrayList;
import java.util.Date;
/**
 *
 * @author Jaume Segarra
 */
public class Voluntario extends Persona{
    private ArrayList<String[]> puestos_desempeño;
    
    public Voluntario(String dni, String nombre, String apellidos, Date fecha_nacimiento, String direccion, int telefono, String sexo, ArrayList<String[]> puestos_desempeño){
        super(dni, nombre,apellidos,fecha_nacimiento, direccion, telefono, sexo);
        this.puestos_desempeño=puestos_desempeño;
    }
    
    public void añadirPuesto(String[] puesto){
        if(!puestos_desempeño.contains(puesto))
            puestos_desempeño.add(puesto);
    }
    
    public void eliminarPuesto(String[] puesto){
        if(puestos_desempeño.contains(puesto))
            puestos_desempeño.remove(puesto);
    }
    
    public ArrayList<String[]> getPuestos(){
        return puestos_desempeño;
    }
}
