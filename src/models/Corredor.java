/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author jaumesegarra
 */
public class Corredor extends Persona{
    private int edad;
    private int dorsal;
    private Club club;
    private int puesto_llegada;
           
    public Corredor(String dni, String nombre, String apellidos, Date fecha_nacimiento, String direccion, int telefono, String sexo, int edad, Club club, int dorsal, int puesto_llegada){
        super(dni, nombre,apellidos,fecha_nacimiento, direccion, telefono, sexo);
        this.edad=edad;
        this.dorsal=dorsal;
        this.club=club;
        this.puesto_llegada=puesto_llegada;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public int getPuesto_llegada() {
        return puesto_llegada;
    }

    public void setPuesto_llegada(int puesto_llegada) {
        this.puesto_llegada = puesto_llegada;
    }
    
    public String[] getCarrera(){
        String[] carrera = new String[2];
        
        if(edad<=4)
            carrera[0] = "chupetines";
        else if(edad>=5 && edad<=8)
            carrera[0] = "minibenjamines";
        else if(edad>=9 && edad<=10)
            carrera[0]= "Benjamín";
        else if(edad>=11 && edad<=12)
            carrera[0]= "Alevín";
        else if(edad>=13 && edad<=14)
            carrera[0]= "Infantil";
        else if(edad>=15 && edad<=16)
            carrera[0]= "Cadete";
        else if(edad>=17 && edad<=19)
            carrera[0]= "Juvenil";
        else if(edad>=20 && edad<=39)
            if((super.getSexo().equals("MUJER")) && edad>34)
                carrera[0]= "Veterano";
            else
                carrera[0]= "Senior";
        else if(edad>=40)
            carrera[0]= "Veterano";
        
        carrera[1] = super.getSexo();
        return carrera;
    }
    
    
    public String toString(){
        String cadena ="";
        cadena += super.toString();
        cadena += "Edad: "+this.edad+"\n";
        String[] car = this.getCarrera();
        cadena += "Club: "+((this.club != null) ? this.club.getNombre() : "--")+"\n";
        cadena += "Carrera correspondiente: "+car[0]+" ("+car[1]+") \n";
        cadena += "Dorsal: "+this.dorsal+"\n";
        cadena += "Posición final: "+this.puesto_llegada+"\n";
        return cadena;
    }
}
