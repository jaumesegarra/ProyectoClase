/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class Patrocinador {
    private String nombre;
    private String direccion;
    private String persona_contacto;
    private int telefono;
    
    public Patrocinador(String nombre, String direccion, String persona_contacto, int telefono){
        this.nombre=nombre;
        this.direccion=direccion;
        this.persona_contacto=persona_contacto;
        this.telefono=telefono;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String getDireccion(){
        return direccion;
    }
    
    public String getPersonaContacto(){
        return persona_contacto;
    }
    
    public int getTelefono(){
        return telefono;
    }
    
    public String toString(){
        String t= "";
        t+="Nombre: "+this.nombre+"\n";
        t+="Dirección: "+this.direccion+"\n";
        t+= "Teléfono: "+this.telefono+"\n";
        t+= "Persona de contacto: "+this.persona_contacto+"\n";
        return t;
    }
}
