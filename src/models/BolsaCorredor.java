/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author jaumesegarra
 */
public class BolsaCorredor {
    private String categoriaCarrera;
    private String sexoCarrera;
    private ArrayList<GrupoRegalo> grupoRegalos;
    
    public BolsaCorredor(String categoriaCarrera, String sexoCarrera){
        this.categoriaCarrera=categoriaCarrera;
        this.sexoCarrera=sexoCarrera;
        this.grupoRegalos=new ArrayList<GrupoRegalo>();
    }
    
    public BolsaCorredor(String categoriaCarrera, String sexoCarrera, ArrayList<GrupoRegalo> grupoRegalos){
        this.categoriaCarrera=categoriaCarrera;
        this.sexoCarrera=sexoCarrera;
        this.grupoRegalos=grupoRegalos;
    }

    public String getCategoriaCarrera() {
        return categoriaCarrera;
    }

    public String getSexoCarrera() {
        return sexoCarrera;
    }
    
    public ArrayList<GrupoRegalo> getGrupoRegalos(){
        return grupoRegalos;
    }
}
