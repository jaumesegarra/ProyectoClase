
import dao.BolsaCorredorDAO;
import models.BolsaCorredor;
import models.Regalo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaumesegarra
 */
public class BolsaCorredor_test {
    public static void main(String[] args) {
        //BolsaCorredor b = BolsaCorredor.load("Benjamín", "HOMBRE");
        for (Regalo reg : BolsaCorredorDAO.darRegalosCorredor("Benjamín", "HOMBRE")){
            System.out.println(reg.getAportacion().getConcepto()+" "+reg.getAportacion().getCantidadDisponible());
        }
    }
}
