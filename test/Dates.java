
import dao.CarreraDAO;
import java.util.Calendar;
import java.util.GregorianCalendar;
import models.Carrera;
import utils.Check;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaumesegarra
 */
public class Dates {
    public static void main(String[] args) {
        GregorianCalendar fecha_actual = new GregorianCalendar();
        
        Carrera car = CarreraDAO.load("Benjamín", "HOMBRE");
        String[] hora_comienzo = car.getHoraComienzo().split(":");
                    
        System.out.println(Check.isEq(fecha_actual, car.getFecha()));
        
        System.out.println(fecha_actual.get(Calendar.HOUR_OF_DAY));
        System.out.println(new Integer(hora_comienzo[0]));
        
        System.out.println(fecha_actual.get(Calendar.HOUR_OF_DAY)*60+fecha_actual.get(Calendar.MINUTE)-30);
        
        System.out.println(new Integer(hora_comienzo[0])*60+new Integer(hora_comienzo[1]));
        
        System.out.println(Check.isEq(fecha_actual, car.getFecha()) && (fecha_actual.get(Calendar.HOUR_OF_DAY)*60+fecha_actual.get(Calendar.MINUTE)+30 >= new Integer(hora_comienzo[0])*60+new Integer(hora_comienzo[1])));
        
        System.out.println(Check.isGt(fecha_actual, car.getFecha()));
    }
}
