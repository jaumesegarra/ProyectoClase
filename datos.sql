/*
	Insertar datos de prueba
*/

INSERT INTO Recorrido (codigo, descripcion, kilometros, aprobada)
VALUES
	('U56','Desde la calle Lepanto a la avenida Doctor Marco',16.50,1),
	('X99','Desde la calle Aparicio hasta la calle de la paz',8.00,0);

INSERT INTO RecorridoObservacion (codigoRecorrido, observacion)
VALUES
	('U56','Parece bastante segura'),
	('X99','Debería limpiarse');

INSERT INTO Carrera (hora_comienzo, categoria, sexo, codigoRecorrido, aprobada)
VALUES
	('10:30','Benjamín','HOMBRE','U56',1),
	('20:15','Benjamín','MUJER','U56',0),
	('20:00','Infantil','HOMBRE','X99',0);

INSERT INTO Regla (id, nombre, descripcion)
VALUES
	(1,'Prohibido fumar','Queda totalmente prohibido fumar en el recinto y alrededores'),
	(2,'Denegada entrada a animales','No se podrá acceder a ninguna carrera ni acto con algun animal'),
	(3,'Prohibido ir en chanclas',':)');

INSERT INTO ReglaCarrera (categoriaCarrera, sexoCarrera, idRegla)
VALUES
	('Benjamín','HOMBRE',1),
	('Benjamín','MUJER',1),
	('Benjamín','HOMBRE',2),
	('Infantil','HOMBRE',3);

INSERT INTO Patrocinador (nombre, direccion, persona_contacto, telefono)
VALUES
	('Jamones Negros S.L','C/ Maldita n23','Pepe',963925976),
	('Tintoreria MariJose','Av. del colegio n12','Maria José',963925678),
	('Toys Manuel','C/ Divertida n23','Manuel Rodriguez',963975465);


INSERT INTO Donacion (id, nombrePatrocinador, concepto)
VALUES
	(1,'Jamones Negros S.L','Patrocinio'),
	(2,'Jamones Negros S.L','Jamón Curado'),
	(3,'Tintoreria MariJose','Pañuelos Rojos'),
	(4,'Tintoreria MariJose','Pañuelos Amarillos'),
	(5,'Toys Manuel','Pistola de agua'),
	(6,'Toys Manuel','Pulsera contadora de pasos');


INSERT INTO DonacionDinero (idDonacion, cantidad)
VALUES
	(1,1200.00);


INSERT INTO DonacionObjeto (idDonacion, cantidad, cantidad_disponible)
VALUES
	(2,20,20),
	(3,50,50),
	(4,50,50),
	(5,120,120),
	(6,250,250);


INSERT INTO RegaloBolsaCorredor (id, periodo, categoriaCarrera, sexoCarrera)
VALUES
	(9,0.50,'Benjamín','HOMBRE'),
	(10,0.50,'Benjamín','MUJER'),
	(11,0.20,'Benjamín','HOMBRE'),
	(12,0.20,'Benjamín','MUJER'),
	(13,1.00,'Infantil','HOMBRE'),
	(14,1.00,'Benjamín','HOMBRE'),
	(15,1.00,'Benjamín','MUJER'),
	(16,1.00,'Infantil','HOMBRE');

INSERT INTO RegaloBolsaCorredorOpcion (id, idRegaloBolsaCorredor, idDonacion, cantidad, cantidad_disponible, cantidad_x_participante, periodo_actual)
VALUES
	(5,9,3,20,20,1,0.00),
	(6,9,4,20,20,1,0.50),
	(7,10,3,20,20,1,0.00),
	(8,10,4,20,20,1,0.50),
	(9,11,2,5,5,1,0.00),
	(10,12,2,5,5,1,0.00),
	(11,13,5,60,60,1,0.00),
	(12,14,6,50,50,1,0.00),
	(13,15,6,50,50,1,0.00),
	(14,16,6,25,25,1,0.00);
