﻿CREATE TABLE Recorrido(
    codigo VARCHAR(10) PRIMARY KEY,
    descripcion VARCHAR(250) NOT NULL,
    kilometros DECIMAL(4,2) NOT NULL,
    aprobada INT(1) DEFAULT 0 NOT NULL
);

CREATE TABLE RecorridoObservacion(
	codigoRecorrido VARCHAR(10),
    observacion VARCHAR(250),
    CONSTRAINT PK_RecorridoObservaciones PRIMARY KEY (codigoRecorrido, observacion),
    CONSTRAINT CAj_codigoRecorrido FOREIGN KEY (codigoRecorrido) REFERENCES Recorrido(codigo) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Carrera (
  	hora_comienzo VARCHAR(5) NOT NULL,
  	categoria ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
  	sexo ENUM('HOMBRE','MUJER') DEFAULT NULL,
  	codigoRecorrido VARCHAR(10) NOT NULL,
    aprobada INT(1) DEFAULT 0 NOT NULL,
    CONSTRAINT PK_carrera PRIMARY KEY (categoria,sexo),
    CONSTRAINT CAj_recorrido FOREIGN KEY (codigoRecorrido) REFERENCES Recorrido(codigo) ON UPDATE CASCADE ON DELETE RESTRICT,
    UNIQUE (hora_comienzo, codigoRecorrido)
);

CREATE TABLE Regla (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(45) NOT NULL,
    descripcion VARCHAR(250)
);

CREATE TABLE ReglaCarrera (
    categoriaCarrera ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
    sexoCarrera ENUM('HOMBRE','MUJER') DEFAULT NULL,
    idRegla INT(11) NOT NULL,
    CONSTRAINT cp_ReglaCarrera PRIMARY KEY (categoriaCarrera, sexoCarrera, idRegla),
    CONSTRAINT cAj_codigoCarrera FOREIGN KEY (categoriaCarrera, sexoCarrera) REFERENCES Carrera(categoria, sexo) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT cAj_idRegla FOREIGN KEY (idRegla) REFERENCES Regla(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Patrocinador (
	nombre VARCHAR(50) PRIMARY KEY,
	direccion TEXT NOT NULL,
	persona_contacto VARCHAR(50),
	telefono INT(9)
);

CREATE TABLE Donacion (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	nombrePatrocinador VARCHAR(50) NOT NULL,
	concepto VARCHAR(120) NOT NULL,
	CONSTRAINT cAj_nombrePatrocinador FOREIGN KEY (nombrePatrocinador) REFERENCES Patrocinador(nombre) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE DonacionDinero (
	idDonacion INT(11),
	cantidad DECIMAL(6,2) NOT NULL,
	CONSTRAINT CP_DonacionDinero PRIMARY KEY (idDonacion),
	CONSTRAINT cAj_nombrePatrocinadorconceptoDonacion FOREIGN KEY (idDonacion) REFERENCES Donacion(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE DonacionObjeto (
	idDonacion INT(11),
	cantidad INT(4) NOT NULL,
	cantidad_disponible INT(4) NOT NULL,
	CONSTRAINT CP_DonacionObjeto PRIMARY KEY (idDonacion),
	CONSTRAINT cAj_nombrePatrocinadorconceptoDonacion2 FOREIGN KEY (idDonacion) REFERENCES Donacion(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RegaloBolsaCorredor(
	id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	categoriaCarrera ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
    sexoCarrera ENUM('HOMBRE','MUJER') DEFAULT NULL,
	periodo decimal(3,2) NOT NULL
);

CREATE TABLE RegaloBolsaCorredorOpcion (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idRegaloBolsaCorredor int(11) NOT NULL,
  idDonacion int(11) NOT NULL,
  cantidad int(4) DEFAULT NULL,
  cantidad_disponible int(4) DEFAULT NULL,
  cantidad_x_participante int(4) DEFAULT 1,
  periodo_actual decimal(3,2) NOT NULL,
  UNIQUE KEY (idRegaloBolsaCorredor, periodo_actual),
  CONSTRAINT cAj_DonacionID FOREIGN KEY (idDonacion) REFERENCES DonacionObjeto (idDonacion) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT cAj_RegaloBolsaCorredorID FOREIGN KEY (idRegaloBolsaCorredor) REFERENCES RegaloBolsaCorredor (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Persona (
  dni varchar(9) PRIMARY KEY,
  nombre varchar(50) NOT NULL,
  apellidos varchar(95) NOT NULL,
  sexo ENUM('HOMBRE','MUJER') DEFAULT NULL,
  fecha_nacimiento DATE NOT NULL,
  direccion varchar(120) NOT NULL,
  telefono int(11) DEFAULT NULL
);

CREATE TABLE Club (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(80),
	direccion VARCHAR(120) NOT NULL,
	telefono INT(9) NOT NULL,
	nombre_responsable VARCHAR(80)
);

CREATE TABLE Corredor (
	dniPersona varchar(9) PRIMARY KEY,
  	dorsal int(3) AUTO_INCREMENT UNIQUE,
  	idClub INT(11) DEFAULT NULL,
  	puesto_llegada INT(4) DEFAULT NULL,
  	CONSTRAINT cAj_PersonaDNI FOREIGN KEY (dniPersona) REFERENCES Persona(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  	CONSTRAINT cAj_ClubNOMBRE FOREIGN KEY (idClub) REFERENCES Club(id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE VIEW CorredorEdadVista AS 
	SELECT dni, FLOOR(DATEDIFF(CURRENT_DATE, p.fecha_nacimiento)/365) AS edad FROM Persona p WHERE dni IN (SELECT dniPersona FROM Corredor);

CREATE TABLE Voluntario(
	dniPersona varchar(9) PRIMARY KEY,
	CONSTRAINT cAj_PersonaDNI2 FOREIGN KEY (dniPersona) REFERENCES Persona(dni) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PuestoVoluntario(
	dniPersonaVoluntario varchar(9),
	puesto ENUM('secretaría', 'megafonía', 'salida', 'meta', 'recorrido') DEFAULT NULL,
	cometido VARCHAR(120) NOT NULL,
	CONSTRAINT CP_PuestoVoluntario PRIMARY KEY(dniPersonaVoluntario, puesto),
	CONSTRAINT cAj_PersonaVoluntarioDNI FOREIGN KEY (dniPersonaVoluntario) REFERENCES Voluntario(dniPersona) ON DELETE CASCADE ON UPDATE CASCADE
);