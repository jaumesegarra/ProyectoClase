﻿/*
	Creación de tablas y vistas
*/
CREATE TABLE Recorrido(
    codigo VARCHAR(10) PRIMARY KEY,
    descripcion VARCHAR(250) NOT NULL,
    kilometros DECIMAL(4,2) NOT NULL,
    aprobada INT(1) DEFAULT 0 NOT NULL
);

CREATE TABLE RecorridoObservacion(
	codigoRecorrido VARCHAR(10),
    observacion VARCHAR(250),
    CONSTRAINT PK_RecorridoObservaciones PRIMARY KEY (codigoRecorrido, observacion),
    CONSTRAINT CAj_codigoRecorrido FOREIGN KEY (codigoRecorrido) REFERENCES Recorrido(codigo)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE TABLE Carrera (
  	hora_comienzo VARCHAR(5) NOT NULL,
  	categoria ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
  	sexo ENUM('HOMBRE','MUJER') DEFAULT NULL,
  	codigoRecorrido VARCHAR(10) NOT NULL,
    aprobada INT(1) DEFAULT 0 NOT NULL,
    CONSTRAINT PK_carrera PRIMARY KEY (categoria,sexo),
    CONSTRAINT CAj_recorrido FOREIGN KEY (codigoRecorrido) REFERENCES Recorrido(codigo)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    UNIQUE (hora_comienzo, codigoRecorrido)
);

CREATE TABLE Regla (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(45) NOT NULL,
    descripcion VARCHAR(250)
);

CREATE TABLE ReglaCarrera (
    categoriaCarrera ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
    sexoCarrera ENUM('HOMBRE','MUJER') DEFAULT NULL,
    idRegla INT(11) NOT NULL,
    CONSTRAINT cp_ReglaCarrera PRIMARY KEY (categoriaCarrera, sexoCarrera, idRegla),
    CONSTRAINT cAj_codigoCarrera FOREIGN KEY (categoriaCarrera, sexoCarrera) REFERENCES Carrera(categoria, sexo) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT cAj_idRegla FOREIGN KEY (idRegla) REFERENCES Regla(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE Patrocinador (
	nombre VARCHAR(50) PRIMARY KEY,
	direccion TEXT NOT NULL,
	persona_contacto VARCHAR(50),
	telefono INT(9)
);

CREATE TABLE Donacion (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	nombrePatrocinador VARCHAR(50) NOT NULL,
	concepto VARCHAR(120) NOT NULL,
	CONSTRAINT cAj_nombrePatrocinador FOREIGN KEY (nombrePatrocinador) REFERENCES Patrocinador(nombre) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE DonacionDinero (
	idDonacion INT(11),
	cantidad DECIMAL(6,2) NOT NULL,
	CONSTRAINT CP_DonacionDinero PRIMARY KEY (idDonacion),
	CONSTRAINT cAj_nombrePatrocinadorconceptoDonacion FOREIGN KEY (idDonacion) REFERENCES Donacion(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE DonacionObjeto (
	idDonacion INT(11),
	cantidad INT(4) NOT NULL,
	cantidad_disponible INT(4) NOT NULL,
	CONSTRAINT CP_DonacionObjeto PRIMARY KEY (idDonacion),
	CONSTRAINT cAj_nombrePatrocinadorconceptoDonacion2 FOREIGN KEY (idDonacion) REFERENCES Donacion(id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE RegaloBolsaCorredor(
	id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	categoriaCarrera ENUM('Benjamín','Alevín','Infantil','Cadete','Juvenil','Senior','Veterano','minibenjamines','chupetines') DEFAULT NULL,
    sexoCarrera ENUM('HOMBRE','MUJER') DEFAULT NULL,
	periodo decimal(3,2) NOT NULL
);

CREATE TABLE RegaloBolsaCorredorOpcion (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idRegaloBolsaCorredor int(11) NOT NULL,
  idDonacion int(11) NOT NULL,
  cantidad int(4) DEFAULT NULL,
  cantidad_disponible int(4) DEFAULT NULL,
  cantidad_x_participante int(4) DEFAULT 1,
  periodo_actual decimal(3,2) NOT NULL,
  UNIQUE KEY (idRegaloBolsaCorredor, periodo_actual),
  CONSTRAINT cAj_DonacionID FOREIGN KEY (idDonacion) REFERENCES DonacionObjeto (idDonacion) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT cAj_RegaloBolsaCorredorID FOREIGN KEY (idRegaloBolsaCorredor) REFERENCES RegaloBolsaCorredor (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE Persona (
  dni varchar(9) PRIMARY KEY,
  nombre varchar(50) NOT NULL,
  apellidos varchar(95) NOT NULL,
  sexo ENUM('HOMBRE','MUJER') DEFAULT NULL,
  fecha_nacimiento DATE NOT NULL,
  direccion varchar(120) NOT NULL,
  telefono int(11) DEFAULT NULL
);

CREATE TABLE Club (
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(80),
	direccion VARCHAR(120) NOT NULL,
	telefono INT(9) NOT NULL,
	nombre_responsable VARCHAR(80)
);

CREATE TABLE Corredor (
	dniPersona varchar(9) PRIMARY KEY,
  	dorsal int(3) AUTO_INCREMENT UNIQUE,
  	idClub INT(11) DEFAULT NULL,
  	puesto_llegada INT(4) DEFAULT NULL,
  	CONSTRAINT cAj_PersonaDNI FOREIGN KEY (dniPersona) REFERENCES Persona(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  	CONSTRAINT cAj_ClubNOMBRE FOREIGN KEY (idClub) REFERENCES Club(id) ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE VIEW CorredorEdadVista AS
	SELECT dni, FLOOR(DATEDIFF(CURRENT_DATE, p.fecha_nacimiento)/365) AS edad FROM Persona p WHERE dni IN (SELECT dniPersona FROM Corredor);

CREATE TABLE Voluntario(
	dniPersona varchar(9) PRIMARY KEY,
	CONSTRAINT cAj_PersonaDNI2 FOREIGN KEY (dniPersona) REFERENCES Persona(dni) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE PuestoVoluntario(
	dniPersonaVoluntario varchar(9),
	puesto ENUM('secretaría', 'megafonía', 'salida', 'meta', 'recorrido') DEFAULT NULL,
	cometido VARCHAR(120) NOT NULL,
	CONSTRAINT CP_PuestoVoluntario PRIMARY KEY(dniPersonaVoluntario, puesto),
	CONSTRAINT cAj_PersonaVoluntarioDNI FOREIGN KEY (dniPersonaVoluntario) REFERENCES Voluntario(dniPersona) ON DELETE CASCADE ON UPDATE CASCADE
);

/*
	Insertar datos de prueba
*/

INSERT INTO Recorrido (codigo, descripcion, kilometros, aprobada)
VALUES
	('U56','Desde la calle Lepanto a la avenida Doctor Marco',16.50,1),
	('X99','Desde la calle Aparicio hasta la calle de la paz',8.00,0);

INSERT INTO RecorridoObservacion (codigoRecorrido, observacion)
VALUES
	('U56','Parece bastante segura'),
	('X99','Debería limpiarse');

INSERT INTO Carrera (hora_comienzo, categoria, sexo, codigoRecorrido, aprobada)
VALUES
	('10:30','Benjamín','HOMBRE','U56',1),
	('20:15','Benjamín','MUJER','U56',0),
	('20:00','Infantil','HOMBRE','X99',0);

INSERT INTO Regla (id, nombre, descripcion)
VALUES
	(1,'Prohibido fumar','Queda totalmente prohibido fumar en el recinto y alrededores'),
	(2,'Denegada entrada a animales','No se podrá acceder a ninguna carrera ni acto con algun animal'),
	(3,'Prohibido ir en chanclas',':)');

INSERT INTO ReglaCarrera (categoriaCarrera, sexoCarrera, idRegla)
VALUES
	('Benjamín','HOMBRE',1),
	('Benjamín','MUJER',1),
	('Benjamín','HOMBRE',2),
	('Infantil','HOMBRE',3);

INSERT INTO Patrocinador (nombre, direccion, persona_contacto, telefono)
VALUES
	('Jamones Negros S.L','C/ Maldita n23','Pepe',963925976),
	('Tintoreria MariJose','Av. del colegio n12','Maria José',963925678),
	('Toys Manuel','C/ Divertida n23','Manuel Rodriguez',963975465);


INSERT INTO Donacion (id, nombrePatrocinador, concepto)
VALUES
	(1,'Jamones Negros S.L','Patrocinio'),
	(2,'Jamones Negros S.L','Jamón Curado'),
	(3,'Tintoreria MariJose','Pañuelos Rojos'),
	(4,'Tintoreria MariJose','Pañuelos Amarillos'),
	(5,'Toys Manuel','Pistola de agua'),
	(6,'Toys Manuel','Pulsera contadora de pasos');


INSERT INTO DonacionDinero (idDonacion, cantidad)
VALUES
	(1,1200.00);


INSERT INTO DonacionObjeto (idDonacion, cantidad, cantidad_disponible)
VALUES
	(2,20,20),
	(3,50,50),
	(4,50,50),
	(5,120,120),
	(6,250,250);


INSERT INTO RegaloBolsaCorredor (id, periodo, categoriaCarrera, sexoCarrera)
VALUES
	(9,0.50,'Benjamín','HOMBRE'),
	(10,0.50,'Benjamín','MUJER'),
	(11,0.20,'Benjamín','HOMBRE'),
	(12,0.20,'Benjamín','MUJER'),
	(13,1.00,'Infantil','HOMBRE'),
	(14,1.00,'Benjamín','HOMBRE'),
	(15,1.00,'Benjamín','MUJER'),
	(16,1.00,'Infantil','HOMBRE');

INSERT INTO RegaloBolsaCorredorOpcion (id, idRegaloBolsaCorredor, idDonacion, cantidad, cantidad_disponible, cantidad_x_participante, periodo_actual)
VALUES
	(5,9,3,20,20,1,0.00),
	(6,9,4,20,20,1,0.50),
	(7,10,3,20,20,1,0.00),
	(8,10,4,20,20,1,0.50),
	(9,11,2,5,5,1,0.00),
	(10,12,2,5,5,1,0.00),
	(11,13,5,60,60,1,0.00),
	(12,14,6,50,50,1,0.00),
	(13,15,6,50,50,1,0.00),
	(14,16,6,25,25,1,0.00);
